## OPTIONS AND VARIABLES ##
# Executables
JC   := javac
JR   := java
RM   := rm -vf --
TAGS := ctags

# Source directory
SRC := src

# Compiler flags
SOURCEPATH := -sourcepath $(SRC)
CLASSPATH  := -classpath src
OUTDIR     := -d $(SRC)
WARNINGS   := -Xlint
JFLAGS     := $(WARNINGS) $(OUTDIR) $(SOURCEPATH) $(CLASSPATH)

# Runtime flags, in case we have a target that executes tests.
JVM_FLAGS := $(CLASSPATH)

# Source files sans suffix go here.
MAIN  := Main
FILES := $(addprefix $(SRC)/, \
		AdminSession          \
		CustomerSession       \
		CSVLoader             \
		Main                  \
		Menu                  \
		Util                  \
	)
CLASSES := $(addsuffix .class, $(FILES))
# Declared for later use. Values are dynamically generated.
LIST :=

## RECIPES ##
.PHONY : classes clean debug default tags

default : classes

debug : JFLAGS += -g

# Only recompiles classes that have changed.
classes : $(CLASSES)
	@$(if $(LIST), \
	echo Compiling: $(basename $(notdir $(LIST))); \
	$(JC) $(JFLAGS) $(LIST) ;)

$(CLASSES) : %.class : %.java
	$(eval LIST += $$<)

run :
	$(JR) $(JVM_FLAGS) $(MAIN)

tags :
	$(TAGS) $(SRC)/*.java sql/*.sql

clean :
	$(RM) $(OUTDIR)/*.class
