# Project Goal
The primary goal of this project was to implement a flight reservation system
for “PittTours,” the imaginary multi-billion travel agency company of the
University of Pittsburgh. The core of such a system is a database system and
a set of ACID transactions. The secondary goal was to learn how to work as a
member of a team which designs and develops a relatively large, real database
application. We were given the descriptions and schemas of the database on
which the system is based. However, we needed to design a text-based interface
which supports all the tasks using Java, Oracle PL/SQL and JDBC. The assignment
focuses on the database component and not on the user interface.

Our implementation uses an MVC design where the model is backing database,
the view is the Menu class, and the controllers are the AdminSession and
CustomerSession classes. The controllers manipulate the model via a database
connection, as one would expect. The view is contained in the session that is
launched by the Main class (depending on whether a customer or administrator)
is using the application, and the Session objects are simple Runnables with a
user I/O loop. Originally, the view was injected into the controllers, but this
flexibility proved unnecessary. Java code is located in the `src/` directory.

The database component required the use of a considerable array of database
features, including integrity constraints, triggers, and stored procedures to
enforce correctness, transactions at various levels of isolation to enable
concurrent use, and of course a number of moderately complex queries. SQL and
PL/SQL code are located in the `sql/` directory. Data generation and testing
scripts are located in the `scripts/` directory.

The application supports some degree of concurrent use, though due to time
constraints it has not been thoroughly tested in that area.

The final project comprises about 2.5k source lines of SQL, 2.2k of Java, and
350 of Perl for data generation and testing, the product of about 2-4 weeks of
work by two developers. Collaboration was done primarily on GitLab, where our
workflow consisted of creating issues for each feature in the issue tracker,
working on them in feature branches, and submitting pull requests to merge them
cleanly into `master`. This approach was very effective for rapid development.
