# 2016-11-14
As said in class today, for this project your TA and I are the "client" and
you are the "service provided." After the initial release of our requirements
and our discussions with few of you, at this point we would like to refine the
requirements a bit further and remove several ambiguities.

1. Specify as a (composite) primary key for Plane, `(plane_type, owner_id)`.
2. Re. Trigger 2 (planeUpgrade), an upgrade will succeed as long as the airline
   has plane (type) with bigger capacity than the currently assigned plane for a
   flight, that is, the currently assigned plane is not of the highest capacity
   plane type.
3. The definition of the primary key for Price `(departure_city, arrival_city)`
   indicates that each route is serviced by only one airline. The provided
   sample data are consistent with this assumption. However, optionally,
   you can specify as a (composite) primary key for Price `(departure_city,
   arrival_city, airline_id)` to allow multiple airlines to service a given
   route.
4. Add to the Reservation Table, `Start_City` and `End_City` of the three letter
   airport code.
5. Assume that a one way trip between two end-points might be composed by one or
   two legs, i.e., it can have at most one connection at a city. In the case,
   of a round-trip, the trip might be composed by two legs (i.e., direct flight
   both ways), three legs (i.e., one direction has a connection) or four legs
   (i.e., both direction have a connection).
6. The price of a ticket/reservation is computed using the high/low price of
   the direct flight between the end-points. So Trigger 1 (adjustTicket) will
   be activated to adjust the cost of a reservation when the price of the
   direct flight between its end-points changes before ticketing. Note that
   with respect to pricing, the start and end dates of the individual legs in a
   reservation are only used in selecting high or low price between end-points
   when computing the cost of a reservation/ticket.

Example, Assuming PIT-DCA $100/$50 (High/Low), PIT-JFK $75/$50 and JFK-DCA
$80/$50.

The cost of a reservation PIT-JFK-DCA will be $100 at high price [the high price
of PIT-DCA] and $50 at low price [the low price of PIT-DCA].

If now either the high/low price of PIT-DCA changes, the this reservation
cost is adjusted by Trigger 1 accordingly.

7. The `credit_card_num` in Reservation seems to be redundant since there is
   a credit card info recorded in Customer. The meaning of these is that the
   `credit_card_num` in Customer is the preferred credit card of the customer
   and the one in Reservation is the finally used credit card to pay for the
   ticket, which may or may not be the preferred credit card.
8. Typo: Ignore the note about headquarter of an airline (p.1)

I hope that I have answered all the questions you have asked me and that with
these clarifications you will be able to easily and successfully complete the
first milestone.

# 2016-11-09
Note that the special table which keeps track of our system time is specified
as `Date(c_date)`. However, date is a keyword in Oracle. Please use the name
`Our_Date(c_date)` to avoid any other conflicts.

If you have already renamed it to something different, such as
`System_Date(c_date)`, it is fine, no worries.

---

One more clarification related to item 2. Since now the Plane PK is
`(plane_type, owner_id)`, you need to adjust the foreign key of Flight to
`FK(plane_type, airline_id) -> Plane(plane_type,owner_id)`.

# 2016-11-15
As a follow up of my yesterday's email, I got the following question:

Do all the legs have to be on the same airline?

1. Round trips could involved different airlines. However, all the legs of a
one-way or round trip with a connection in the same direction must be on the
same airline. In this way, the price of each direction will be based on the
price of only one airline.
2. We assume that if an airline supports a trip with connections between two
cities, it also support a direct flight between these two cities.

# 2016-11-21
## Xiaoyu:
This is to clarify that for the project when you incorporated the sample input
that we have provided in the description, here, for each value that belongs to
a different attribute it should be separated by a comma. This way it would be
easier for you to parse the inputs.

## Panos:
Let me make more clear Xiaoyu's project clarification with an example.

In the handout, Task #2 uploads data from an input file. The file will be a CSV
one:

    001,United Airlines,UAL,Chicago,1931
    002,All Nippon Airways,ANA,Tokyo,1952
    003,Delta Air Lines,DAL,Georgia,1924
