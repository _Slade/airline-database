package RandUtils;
use strict;
use warnings;
use Carp qw(croak);
use Exporter qw(import);

our @EXPORT_OK = qw(
    pick
    pick_key
    rand_date
    rand_date_iso8601
    rand_date_vernacular
    rand_flag
    rand_schedule
    rand_time
);

use constant DAYS => qw(31 29 31 30 31 30 31 31 30 31 30 31);

sub rand_date {
    my ($min_year, $max_offset) = @_;
    croak 'Need minimum year' if not defined $min_year;
    croak 'Year less than zero' if $min_year < 0;
    croak 'Offset must be nonnegative' if $max_offset < 0;
    my $year  = $min_year + int rand $max_offset;
    my $month = 1 + int rand 12;
    my $day   = 1 + int rand((DAYS)[$month - 1]);
    return $year, $month, $day;
}

sub rand_date_vernacular {
    my ($year, $month, $day) = rand_date(@_);
    return sprintf '%02d/%02d/%04d', $month, $day, $year;
}

sub rand_date_iso8601 {
    my ($year, $month, $day) = rand_date(@_);
    return sprintf '%04d-%02d-%02d', $year, $month, $day;
}

sub pick {
    return $_[0]->[ int rand @{$_[0]} ];
}

sub pick_key {
    my $hashref = shift;
    return unless keys %$hashref;
    return (keys %$hashref)[int rand keys %$hashref];
}

sub rand_schedule {
    my @days = qw(S M T W T F S);
    my @sched;
    for (0 .. $#days) {
        push @sched, rand() > 0.5 ? $days[$_] : '-';
    }
    return join '', @sched;
}

sub rand_time {
    my $hour = 100 * (1 + int rand 24);
    my $minutes = 10 * int rand 6;
    return $hour + $minutes;
}

sub rand_flag {
    return rand() < 0.5 ? 'Y' : 'N';
}

1;
