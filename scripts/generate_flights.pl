#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use feature qw(say unicode_strings);
use open qw(:encoding(UTF-8) :std);
use lib qw(scripts);
use Scalar::Util qw(looks_like_number);
use RandUtils qw(to_ymd pick rand_schedule rand_time);
use Cwd;
use Getopt::Long;

=head1 Generate sample flight data.

Example flight tuples:

    153 001 A320 PIT JFK 1000 1120 SMTWTFS
    154 003 B737 JFK DCA 1230 1320 S-TW-FS
    552 003 E145 PIT DCA 1100 1150 SM-WT-S

Examle flight pricing:

    PIT JFK 001 250 120
    JFK PIT 003 250 120
    JFK DCA 003 220 100
    DCA JFK 003 210 90
    PIT DCA 002 200 150
    DCA PIT 001 215 150

=cut

sub usage {
    print <<'EOT';
Usage: generate_flights.pl [--csv] <number of flights to generate>

If the --csv option is specified, prints the output in CSV format. Flight
information will be printed to the file 'flights.csv' and pricing information
to the file 'prices.csv'.

Otherwise the output will be printed to 'flights.sql' and 'prices.sql'.
EOT
    exit 0;
}

# Flight_Number PK
# Airline_ID    FK Airline(Airline_ID)
# Plane_Type    FK Plane(Plane_Type)
# Departure_City
# Arrival_City
# Departure_Time
# Arrival_Time
# Weekly_Schedule
use constant FLIGHT_FMT => join(', ',
    qw('%.3s' '%.5s' '%04.4s' '%.3s' '%.3s' '%04.4d' '%04.4d' '%.7s')
);
use constant FLIGHT_CSV => join(',',
    qw(%.3s %.5s %04.4s %.3s %.3s %04.4d %04.4d %.7s)
);

# Departure_City PK
# Arrival_City   PK
# Airline_ID     FK Airline(Airline_ID)
# High_Price
# Low_Price
use constant PRICE_FMT => join(', ', qw('%.3s' '%.3s' '%.5s' %d %d));
use constant PRICE_CSV => join(',', qw(%.3s %.3s %.5s %d %d));

my @airlines = map { sprintf '%03d', $_ } 1 .. 15;

my @airports = qw(
    BHM DHN HSV MOB MGM ANC ANI BRW BET CDV SCC DLG FAI GAL GST HNS HOM HNH JNU
    ENA KTN AKN ADQ OTZ OME PSG SIT KSM UNK DUT VDZ WRG YAK IFP FLG GCN AZA PGA
);
    # GCW PHX TUS YUM XNA FSM LIT TXK ACV BFL BUR CLD CIC CEC FAT LGB LAX MMH MOD
    # MRY OAK ONT PSP RDD SMF SAN SFO SJC SBP SNA SBA SMX STS SCK ASE COS DEN DRO
    # EGE GJT GUC HDN MTJ BDL HVN DAB FLL RSW GNV JAX EYW MLB MIA MCO SFB ECP PNS
    # PGD SRQ UST PIE TLH TPA VPS PBI ABY ATL AGS BQK CSG SAV VLD ITO HNL OGG KOA
    # MKK LNY LIH BOI SUN IDA LWS PIH TWF BLV BMI CMI ORD MDW MWA MLI PIA UIN RFD
    # SPI EVV FWA IND SBN CID DSM DBQ SUX ALO GCK MHK ICT CVG LEX SDF OWB PAH AEX
    # BTR LFT LCH MLU MSY SHV BGR BHB PWM PQI RKD BWI SBY HGR BOS HYA ACK PVC MVY
    # ORH APN CVX DTW ESC FNT GRR CMX IMT AZO LAN MQT MKG PLN MBS CIU TVC BJI BRD
    # DLH HIB INL MSP RST STC GTR GPT PIB JAN COU JLN MCI SGF STL BIL BZN BTM GTF
    # HLN FCA MSO GRI EAR LNK OMA BFF BLD EKO LAS RNO LEB MHT PSM ACY TTN EWR ABQ
    # FMN HOB ROW SAF ALB BGM BUF ELM ISP ITH JFK LGA SWF IAG PBG ROC SYR ART HPN
    # AVL CLT FAY GSO PGV OAJ EWN RDU ILM BIS DIK FAR GFK MOT ISN CAK LUK CLE CMH
    # LCK DAY TOL YNG LAW OKC TUL EUG LMT MFR OTH PDX RDM ABE ERI MDT LBE PHL PIT
    # SCE AVP IPT PVD WST CHS CAE FLO GSP HHH MYR ABR PIR RAP FSD TRI CHA TYS MEM
    # BNA ABI AMA AUS BPT BRO CLL CRP DAL DFW DRT ELP GRK HRL IAH HOU LRD GGG LBB
    # MFE MAF SJT SAT TYR ACT SPS PVU SLC SGU ENV BTV CHO LYH PHF ORF RIC ROA SHD
    # DCA IAD BLI FRD PSC CLM PUW BFI SEA GEG ALW EAT YKM CRW CKB HTS LWB MGW ATW
    # EAU GRB LSE MSN MKE CWA RHI CPR CYS COD GCC JAC RIW RKS SHR PPG GUM SPN ROP
    # BQN NRR PSE SJU SIG VQS STT STX

die "Please provide the number of tuples to generate\n" unless @ARGV;

GetOptions(
    'csv'  => \my $print_csv,
    'help' => \my $help,
) or die 'Bad option';

usage() if $help;

my @planes;
# Should work when called from scripts directory or from root of Git repo
my $cwd = getcwd();
my $path = $cwd =~ m[scripts/?$]
    ? '../sample_data/planes.csv'
    : './sample_data/planes.csv';
open my $fh, '<', $path;
while (my $line = readline $fh) {
    chomp $line;
    my @vals = split /,/, $line;
    # Plane_type, Owner_ID
    my $plane_type = sprintf '%.4s', $vals[0];
    my $owner_id = sprintf '%.5s', $vals[5];
    if ($plane_type ne $vals[0] or $owner_id ne $vals[5]) {
        warn "Overly long field at $path line $.";
    }
    push @planes, [ map { s/^'|'$//gr } @vals[0, 5] ];
}

my $iters = shift;
die 'Bad argument' unless looks_like_number($iters);
open my $fh1, '>:encoding(UTF-8)',
    $print_csv
    ? 'flights.csv'
    : 'flights.sql' or die $!;
open my $fh2, '>:encoding(UTF-8)',
    $print_csv
    ? 'prices.csv'
    : 'prices.sql' or die $!;
my %routes; # Have to track these so only 1 price is printed per route
for my $i (1 .. $iters) {
    my $flight_num = sprintf '%03d', $i;
    my $airline    = pick(\@airlines);
    my ($plane_type, $airline_id) = @{ pick(\@planes) };
    my $dept_city = pick(\@airports);
    my $arrv_city = pick(\@airports);
    $arrv_city    = pick(\@airports) while $arrv_city eq $dept_city;
    my ($dept_time, $arrv_time) = (rand_time(), rand_time());
    if ($arrv_time < $dept_time) {
        ($dept_time, $arrv_time) = ($arrv_time, $dept_time);
    }
    my $schedule = rand_schedule();
    my $high_price = 10 * int rand 999;
    my $low_price  = int rand $high_price;
    if (not $print_csv) {
        $fh1->printf(
            'INSERT INTO Flight VALUES (' . FLIGHT_FMT . ");\n",
            $flight_num, $airline_id, $plane_type, $dept_city,
            $arrv_city, $dept_time, $arrv_time, $schedule
        );
        $fh2->printf(
            'INSERT INTO Price VALUES (' . PRICE_FMT . ");\n",
            $dept_city, $arrv_city, $airline_id, $high_price, $low_price
        ) unless exists $routes{$dept_city . $arrv_city};
    }
    else {
        $fh1->printf(
            FLIGHT_CSV . "\n",
            $flight_num, $airline_id, $plane_type, $dept_city,
            $arrv_city, $dept_time, $arrv_time, $schedule
        );
        $fh2->printf(
            PRICE_CSV . "\n",
            $dept_city, $arrv_city, $airline_id, $high_price, $low_price
        ) unless exists $routes{$dept_city . $arrv_city};
    }
    # Mark price for this route as printed
    $routes{$dept_city . $arrv_city} = 1;
}
close $_ or die $! for $fh1, $fh2;
