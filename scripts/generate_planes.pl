#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use feature qw(say unicode_strings);
use open qw(:encoding(UTF-8) :std);
use RandUtils qw(rand_date_vernacular);

sub trim {
    my ($s) = @_;
    return undef if !defined $s;
    $s =~ s/^\s+//;
    $s =~ s/\s+\z//;
    $s
}

sub pick {
    $_[0]->[ int(rand(@{$_[0]})) ];
}

use constant FMT => join(',',
    qw(%04.4s %.10s %s %s %s %s)
);

# B737 Boeing 125 09/09/2009 1996 001
# A320 Airbus 155 10/01/2011 2001 001
# E145 Embraer 50 06/15/2010 2008 002
my @owners = map { sprintf '%03d', $_ } 1 .. 15;
my @years = (1990 .. 2030);
my @caps = map { $_ * 10 } 1 .. 30;
my %seen;
while (my $line = readline DATA) {
    chomp($line);
    my ($manu, $name) = split /,/, $line, 2;
    $manu = trim(sprintf('%.10s', $manu));
    $name = trim(sprintf('%.4s', $name));
    next if $seen{$name};
    $seen{$name}++;
    my $year = pick(\@years);
    next unless defined $manu and defined $name;
    printf(
        FMT . "\n",
        $name,
        $manu,
        pick(\@caps),
        rand_date_vernacular($year,
        30),
        $year,
        pick(\@owners)
    );
}

__DATA__
ANEC,III
Airco,DH.16
Airco,DH.4A
Airco,DH.9C
Airspeed,Ambassador
Airspeed,Envoy
Albatros,L 73
Antonov,An-10
Antonov,An-24
Armstrong Whitworth,Argosy
Armstrong Whitworth,Atalanta
Armstrong Whitworth,Ensign
Aviation Traders,Accountant
Aviation Traders,Carvair
Avro,618 Ten
Avro,642 Eighteen
Avro,Canada C102 Jetliner
Avro,Lancastrian
Avro,Tudor
Avro,York
Aerospatiale,Corvette
BAC,1-11
BAT,F.K.26
BFW,M.20
Beechcraft,90 King Air
Beechcraft,A100 King Air
Beechcraft,B100 King Air
Bleriot-SPAD,S.271919
Bleriot-SPAD,S.331920
Bleriot-SPAD,S.46
Bleriot-SPAD,S.56
Boeing,247
Boeing,2707
Boeing,307
Boeing,314
Boeing,377
Boeing,40
Boeing,707
Boeing,727
Boeing,737
Boeing,747
Boeing,80
Boeing,B-17 Flying Fortress
Breda-Zappata,BZ.308
Breguet,26T
Breguet,280T
Breguet,393T
Breguet,941
Breguet,Deux-Ponts
Bristol,Britannia
Bristol,Ten-seater
Bristol,Type 223 (project)
Canadair,North Star
Cessna,402
Consolidated,Commodore
Consolidated,Fleetster
Convair,240
Convair,340
Convair,440
Convair,540
Convair,580
Convair,600
Convair,640
Convair,880
Convair,990
Curtiss,Commando
Curtiss,Condor
Curtiss,Eagle
Curtiss,Kingbird
Curtiss,Robin
Dassault,Mercure
Dewoitine,D.332
Dewoitine,D.338
Dornier,Do X
Douglas,DC-1
Douglas,DC-2
Douglas,DC-3
Douglas,DC-4
Douglas,DC-4E
Douglas,DC-5
Douglas,DC-6
Douglas,DC-7
Douglas,DC-8
Douglas,XCG-17
Embraer,Bandeirante
Farman,F.120
Farman,F.190
Farman,F.300
Farman,F.50P
Farman,F.60 Goliath
Fiat,G.12
Fiat,G.212
Focke-Wulf,A 17
Focke-Wulf,Fw 200
Fokker,F.II
Fokker,F.III
Fokker,F.VII
Fokker,F.XII
Fokker,F.XVIII
Fokker,F.XXII
Fokker,F27 Friendship
Fokker,F28 Fellowship
Ford,Trimotor
Grahame-White,Charabanc
Grumman,Gulfstream I
Handley Page,Dart Herald
Handley Page,H.P.42
Handley Page,Halifax
Handley Page,Halton
Handley Page,Hastings
Handley Page,Herald
Handley Page,Hermes
Handley Page,Type W
Hawker,Siddeley HS 748
Hawker,Siddeley Trident
Hughes,H-4 Hercules
Ilyushin,Il-62
Junkers,F.13
Junkers,G 24
Junkers,G 31
Junkers,Ju 160
Junkers,Ju 52
Junkers,Ju 60
Junkers,Ju 86
Junkers,Ju 90
Junkers,W 34
Kalinin,K-4
Kalinin,K-5
Kawasaki,Ki-56
Lockheed,Air Express
Lockheed,Constellation
Lockheed,Electra
Lockheed,Electra Junior
Lockheed,Hudson
Lockheed,L-1011 TriStar
Lockheed,L-188 Electra
Lockheed,Lodestar
Lockheed,Orion
Lockheed,Super Electra
Lockheed,Vega
Martin,4-0-4
Martin,M-130
McDonnell Douglas,DC-10
McDonnell Douglas,DC-9
Messerschmitt,M 18
Messerschmitt,M 24
Nieuport-Delage,NiD 30
Nieuport-Delage,NiD 39
Nieuport-Delage,NiD 640
Northrop,Alpha
Northrop,Delta
Percival,Pembroke
Percival,Petrel
Piper,PA-31 Navajo
Potez,SEA VII
Riley,Turbo Skyliner
Rohrbach,Roland
SNCASE,Armagnac
SNCASE,Languedoc
Saunders,ST-27
Saunders,ST-28
Saunders-Roe,Princess
Savoia-Marchetti,SM.95
Shin Meiwa,Tawron
Short,Empire
Short,S.26
Short,Sandringham
Sikorsky,S-40
Sikorsky,S-42
Sopwith,Wallaby
Spartan,Cruiser
Stinson,Detroiter
Stinson,Model A
Stinson,Model T
Stinson,Model U
Stinson,Reliant
Sud Aviation,Caravelle
Supermarine,Swan
Travel Air,6000
Tupolev,Tu-104
Tupolev,Tu-114
Tupolev,Tu-144
Tupolev,Tu-154
Udet,U 11 Kondor
VFW-Fokker,614
Vickers,VC-10
Vickers,VC.1 Viking
Vickers,Vanguard
Vickers,Vimy Commercial
Vickers,Viscount
Vickers,Vulcan
Vultee,V-1
Westland,Limousine
Westland,Wessex
Wibault,280
Wibault,360
Yakovlev,Yak-40
Zeppelin-Staaken,E-4/201919
de Havilland Canada,DHC-2 Beaver
de Havilland Canada,DHC-3 Otter
de Havilland Canada,DHC-4 Caribou
de Havilland Canada,DHC-5 Buffalo
de Havilland Canada,DHC-6 Twin Otter
de Havilland,Comet
de Havilland,DH.18
de Havilland,DH.34
de Havilland,DH.50
de Havilland,DH.66
de Havilland,Dove
de Havilland,Dragon
de Havilland,Dragon Rapide
de Havilland,Express
de Havilland,Fox Moth
de Havilland,Hercules
de Havilland,Heron
