#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use feature qw(say unicode_strings);
use open qw(:encoding(UTF-8) :std);
use lib 'scripts';
use Scalar::Util qw(looks_like_number);
use RandUtils qw(rand_date_iso8601 rand_flag pick pick_key);
use Cwd;
use Time::Piece;
use Time::Seconds;

=pod

Reservation(
    Reservation_Number VARCHAR(5),
    CID                VARCHAR(9),
    Cost               INTEGER,     -- Sum of legs of Reservation detail
    Credit_Card_Num    VARCHAR(16),
    Reservation_Date   DATE,
    Ticketed           VARCHAR(1),
    Departure_City     VARCHAR(3), -- Start_City
    Arrival_City       VARCHAR(3), -- End_City
);

Reservation_detail(
    Reservation_Number VARCHAR(5),
    Flight_Number      VARCHAR(3),
    Flight_Date        DATE,
    Leg                INTEGER,
);

After verifying that a particular route has available seats, a reservation can
be made for a particular trip for a customer. Reservation information for a
particular reservation is maintained in two tables.

Information in Reservation table includes:
    * the total cost of the trip
    * the date on which day the reservation was placed
    * a flag (ticketed) indicating whether a ticket has been issued (the value
    is either ‘Y’ or ‘N’).

Information in Reservation detail table includes :
    * all the legs (starting from 0 and increased by 1 for consecutive legs) of
    the flight
    * the dates they are performed.

Each reservation covers an entire trip, i.e., includes the return portion for
round-trip reservations.

We do not allow a customer to cancel or modify an existing reservation, although
unpaid/non-ticketed reservations are cancelled by the system automatically 12
hours prior the departure (see trigger below).

=cut

die 'Need number of reservations to generate' unless @ARGV;
my $iters = shift;
die unless looks_like_number($iters);

sub get_sample_fh {
    my $file = shift;
    my $cwd = getcwd();
    my $path = $cwd =~ m[scripts/?$]
        ? "../sample_data/$file"
        : "./sample_data/$file";
    open my $fh, '<', $path or die $!;
    return $fh;
}

# Need to get the flights to pick random ones
sub get_flights {
    my $flights = shift; # Hash ref
    my $fh = get_sample_fh('flights.csv');
    while (my $line = readline $fh) {
        # Need flight_number, departure_city, arrival_city
        chomp $line;
        my @F = split /,/, $line;
        # from -> to = flight num
        $flights->{$F[3]}{$F[4]} = $F[0];
    }
    close $fh;
}

sub get_customers {
    my $customers = shift; # Array ref
    my $fh = get_sample_fh('customers.sql');
    my $skip = q('[^']+', \s* );
    my $cap  = q('([^']+)', \s* );
    while (my $line = readline $fh) {
        # Need CID and CC, fields 1 and 5
        if ($line =~ m{
            insert \s+ into \s+ customer \s+ values \s* \(
            $cap $skip $skip $skip $cap
        }ix) {
            push @$customers, [ $1, $2 ];
        }
    }
    close $fh;
}

sub print_res {
    my ($rnum, $cid, $cost, $cc_num, $rdate, $ticketed, $depart, $arrive) = @_;
    printf q|INSERT INTO Reservation VALUES(|
        .  q|'%.5d', '%.9s', %s, '%.16s', DATE '%s', '%.1s', '%.3s', '%.3s'|
        . qq|);\n|,
        $rnum, $cid, $cost, $cc_num, $rdate, $ticketed, $depart, $arrive;
}

sub print_res_detail {
    my ($rnum, $flight_num, $flight_date, $leg) = @_;
    printf q|INSERT INTO Reservation_Detail VALUES(|
        .  q|'%.5d', '%.3d', DATE '%s', %d|
        . qq|);\n|,
        $rnum, $flight_num, $flight_date, $leg;
}

my %flights;
get_flights(\%flights);
my @customers;
get_customers(\@customers);

for my $i (1 .. $iters) {
    my $rnum = $i;
    # Cost has to be calculated by the database. Too complicated for this sort
    # of script.
    my ($cid, $cc_num) = @{ pick(\@customers) };
    my $res_date = rand_date_iso8601(2016, 0);

    # Need to calculate reservation date, then a series of flights occurring
    # after it.
    my @legs;
    my $num_legs = 1 + int rand 4; # Only up to 4 legs, just to be realistic.
    my $last_date = $res_date;
    my ($first_flight, $last_flight);
    for my $leg (1 .. $num_legs) {
        my $leg_date = Time::Piece->strptime($last_date, '%Y-%m-%d');
        $leg_date += ONE_DAY * (1 + int rand 7); # Next flight 1 to 7 days later
        # Now need to pick a flight for that time
        if (not defined $last_flight) {
            $last_flight = pick_key(\%flights);
            $first_flight = $last_flight; # Save this for Reservation
        }
        last if not exists $flights{$last_flight};
        my $dest = pick_key($flights{$last_flight});
        push @legs, [
            $rnum,                           # Reservation_Number
            $flights{$last_flight}{$dest},   # Flight_Number
            $leg_date->strftime('%Y-%m-%d'), # Flight_Date
            $leg - 1                         # Leg (starts at 0)
        ];
        $last_flight = $dest;
    }
    print_res(
        $rnum,         # Reservation_Number
        $cid,          # CID
        '0',           # Cost
        $cc_num,       # Credit_Card_Num
        $res_date,     # Reservation_Date
        rand_flag(),   # Ticketed
        $first_flight, # Departure_City
        $last_flight   # Arrival_City
    );

    print_res_detail(@$_) for @legs;
}
