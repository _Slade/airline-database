------------------------------PROCEDURES------------------------------
----------------------------------&-----------------------------------
------------------------------FUNCTIONS-------------------------------
SET TRANSACTION READ WRITE NAME 'Create procedures and functions';
SET CONSTRAINTS ALL DEFERRED;

---------------------------getFlightNumber----------------------------
-- Get the flight number using an arrival city, departure city, and
-- an airline id.
CREATE OR REPLACE FUNCTION fun_get_flight_number (arr_city IN VARCHAR,
                                                  dep_city IN VARCHAR)
RETURN VARCHAR
AS
    flight VARCHAR(3);
BEGIN
    SELECT Flight_Number INTO flight
    FROM Flight
    WHERE Arrival_City   = arr_city
      AND Departure_City = dep_city;
    RETURN (flight);
END;
/

------------------------------LegPrice--------------------------------
-- Check if the price should be high or low.
-- Returns FALSE if it is high or TRUE if it is low.

CREATE OR REPLACE FUNCTION fun_leg_price (reservation_num IN VARCHAR)
RETURN BOOLEAN
AS
-- Get the first departure and
-- the last arrival to determine
-- high or low price.
    first_departure DATE;
    last_departure  DATE;
BEGIN
    -- Get the first departure.
    SELECT Flight_Date INTO first_departure
    FROM (SELECT Flight_Date, Leg
          FROM Reservation_Detail
          WHERE Reservation_Number = reservation_num
          ORDER BY Leg DESC)
    WHERE ROWNUM <= 1;

    -- Get the last departure.
    SELECT Flight_Date INTO last_departure
    FROM (SELECT Flight_Date, Leg
          FROM Reservation_Detail
          WHERE Reservation_Number = reservation_num
          ORDER BY Leg ASC)
    WHERE ROWNUM <= 1;

    -- Return whether it is high or low.
    IF first_departure = last_departure
    THEN
        RETURN FALSE;
    END IF;

    RETURN TRUE;
END;
/

----------------------------adjustCost--------------------------------
-- Set the cost for the reservation.
CREATE OR REPLACE PROCEDURE proc_adjust_cost (reservation_num IN VARCHAR)
AS
    low_or_high   BOOLEAN;
    price         INTEGER;
BEGIN
    -- Get the relevant flights for the reservation.
    FOR detail_rec in (
        SELECT *
        FROM Reservation_Detail
        WHERE Reservation_Number = reservation_num)
    LOOP
        -- Get whether we're using a high or low price.
        --    True  if low.
        --    False if high.
        low_or_high := fun_leg_price (reservation_num);

        IF low_or_high
        THEN
        -- Using the low price.
            SELECT Low_Price INTO price
            FROM Flight_And_Price
            WHERE Flight_Number = detail_rec.Flight_Number;
        ELSE
        -- Using the high price.
            SELECT High_Price INTO price
            FROM Flight_And_Price
            WHERE Flight_Number = detail_rec.Flight_Number;
        END IF;

        UPDATE Reservation
        SET Cost = Cost + price
        WHERE Reservation_Number = reservation_num;
    END LOOP;
END;
/

-----------------------------confirmCost------------------------------
-- Not working yet.
CREATE OR REPLACE PROCEDURE proc_confirm_cost
AS
    CURSOR reservation_cursor IS
        SELECT *
        FROM Reservation;
--        FOR UPDATE OF Cost;
BEGIN
    -- Loop across all reservation numbers.
    -- Print out the values and see if they exist.
    FOR reservation_record IN reservation_cursor
    LOOP
        dbms_output.put_line (reservation_record. Reservation_Number);
--        proc_adjust_cost (reservation_record. Reservation_Number);
    END LOOP;
END;
/

-------------------------getFlightPassengers--------------------------
CREATE OR REPLACE FUNCTION fun_get_passengers (flight_num IN VARCHAR)
RETURN INTEGER
IS
    passengers INTEGER;
BEGIN
    -- Count how many reservations
    -- are for the flight number.
    -- Assume that flight_number occurs
    -- once per reservation.
    SELECT COUNT(Reservation_Number) INTO passengers
    FROM Reservation_Detail
    WHERE Flight_Number = flight_num;

    RETURN (passengers);
END;
/

----------------------------getBiggerPlane----------------------------
-- Works for what I've tested.
CREATE OR REPLACE FUNCTION fun_get_bigger_plane (flight_num IN VARCHAR)
RETURN CHAR
IS
    current_airline  VARCHAR(5);
    current_plane    CHAR(4);
    current_capacity INTEGER;
    return_plane     CHAR(4);

BEGIN
    -- Grab the current plane type 
    -- and owner for the flight.
    SELECT Plane_Type, Airline_ID 
      INTO current_plane, current_airline
    FROM Flight
    WHERE Flight_Number = flight_num;

    -- Grab the current capacity for
    -- the plane.
    SELECT Plane_Capacity INTO current_capacity
    FROM Plane
    WHERE Plane_Type = current_plane
      AND Owner_ID   = current_airline;

    -- Find the next highest plane.
    -- Need to handle the exception
    -- in the case that nothing is found.
    -- Just throw another exception.
    SELECT Plane_Type INTO return_plane
    FROM (
        -- Sort all the bigger planes in
        -- ascending order.
        SELECT Plane_Capacity, Plane_type
        FROM Plane
        WHERE Plane_Capacity > current_capacity
          AND Owner_ID       = current_airline
        ORDER BY Plane_Capacity ASC)
    WHERE ROWNUM <= 1;

    RETURN (return_plane);

EXCEPTION
    -- Should this just fail?
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END;
/

----------------------------getSmallerPlane---------------------------
-- Works for what I've tested.
CREATE OR REPLACE FUNCTION fun_get_smaller_plane (flight_num IN VARCHAR)
RETURN CHAR
IS
    current_airline  VARCHAR(5);
    current_plane    CHAR(4);
    current_capacity INTEGER;
    return_plane     CHAR(4);

BEGIN
    -- Grab the current plane type for
    -- the flight.
    SELECT Airline_ID, Plane_Type
      INTO current_airline, current_plane
    FROM Flight
    WHERE Flight_Number = flight_num;

    -- Grab the current capacity for
    -- the plane.
    SELECT Plane_Capacity INTO current_capacity
    FROM Plane
    WHERE Plane_Type = current_plane
      AND Owner_ID   = current_airline;

    -- Find the next smallest plane.
    -- Need to handle the exception
    -- in the case that nothing is found.
    -- Just throw another exception.
    SELECT Plane_Type INTO return_plane
    FROM (
        -- Sort all the bigger planes in
        -- ascending order.
        SELECT Plane_Capacity, Plane_type
        FROM Plane
        WHERE Plane_Capacity < current_capacity
          AND Owner_ID       = current_airline
        ORDER BY Plane_Capacity DESC)
    WHERE ROWNUM <= 1;

    RETURN (return_plane);

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END;
/

--------------------------changeSmallerPlane--------------------------
-- Switches to a smaller plane if possible.
CREATE OR REPLACE PROCEDURE proc_change_plane (flight_num IN VARCHAR)
AS
    curr_passengers INTEGER;
    curr_plane      CHAR(4);
    new_plane       CHAR(4);
    air_id          VARCHAR(5);
    curr_capacity   INTEGER;
    new_capacity    INTEGER;

BEGIN
    -- Get the current passengers.
    curr_passengers := fun_get_passengers (flight_num);

    -- Grab the current plane type.
    SELECT Plane_Type, Airline_ID INTO curr_plane, air_id
    FROM Flight
    WHERE Flight_Number = flight_num;

    -- Grab a smaller plane.
    new_plane := fun_get_smaller_plane (flight_num);

    IF new_plane IS NOT NULL THEN
        -- Grab the new capacity.
        SELECT Plane_Capacity INTO new_capacity
        FROM Plane
        WHERE Plane_Type = new_plane
          AND Owner_ID   = air_id;

        -- If the curr passengers can fit in a smaller
        -- plane, then switch it.
        IF curr_passengers <= new_capacity THEN
            UPDATE Flight
            SET Plane_Type = new_plane
            WHERE Flight_Number = flight_num;
        END IF;
    END IF;
END;
/

show errors;

------------------------------schedulesShareDay--------------------------------
-- Checks if two schedules share a day

CREATE OR REPLACE FUNCTION schedulesShareADay (
    a IN Flight.Weekly_Schedule%TYPE,
    b IN Flight.Weekly_Schedule%TYPE
)
RETURN INTEGER -- Oracle SQL doesn't understand PL/SQL's BOOLEAN
AS
    i INTEGER;
BEGIN
    /* Compare each character seeing if the corresponding position
     * matches in the other schedule. */
    FOR i IN 0 .. 6 LOOP
        IF SUBSTR(a, i, 1) != '-' AND SUBSTR(a, i, 1) = SUBSTR(b, i, 1) THEN
            RETURN 1;
        END IF;
    END LOOP;

    RETURN 2;
END;
/
COMMIT;
