SET TRANSACTION READ WRITE NAME 'Drop tables';
SET CONSTRAINTS ALL DEFERRED;

DROP TABLE Flight             CASCADE CONSTRAINTS PURGE;
DROP TABLE Plane              CASCADE CONSTRAINTS PURGE;
DROP TABLE Price              CASCADE CONSTRAINTS PURGE;
DROP TABLE Customer           CASCADE CONSTRAINTS PURGE;
DROP TABLE Reservation        CASCADE CONSTRAINTS PURGE;
DROP TABLE Reservation_detail CASCADE CONSTRAINTS PURGE;
DROP TABLE Date_Info          CASCADE CONSTRAINTS PURGE;
DROP TABLE Airline            CASCADE CONSTRAINTS PURGE;

PURGE RECYCLEBIN;

show errors;
COMMIT;

SET TRANSACTION READ WRITE NAME 'Create tables';
SET CONSTRAINTS ALL DEFERRED;

CREATE TABLE Airline(
    Airline_ID           VARCHAR(5),
    Airline_Name         VARCHAR(50),
    Airline_Abbreviation VARCHAR(10),
    Year_Founded         INTEGER,
    CONSTRAINT pk_airline
        PRIMARY KEY(Airline_ID)
        DEFERRABLE INITIALLY IMMEDIATE,
    -- Year must be positive.
    CONSTRAINT airline_year_domain
        CHECK(Year_Founded > 0)
);

CREATE TABLE Plane(
    Plane_Type      CHAR(4),
    Manufacture     VARCHAR(10),
    Plane_Capacity  INTEGER,
    Last_Service    DATE,
    Year            INTEGER,
    Owner_ID        VARCHAR(5),
    CONSTRAINT pk_plane
        PRIMARY KEY(Plane_Type, Owner_ID)
        DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT fk_plane_owner_id
        FOREIGN KEY(Owner_ID) REFERENCES Airline(Airline_ID)
        INITIALLY DEFERRED DEFERRABLE,
    -- Year must be positive.
    CONSTRAINT plane_year_domain
        CHECK(Year > 0),
    -- Planes must be able to carry passengers.
    CONSTRAINT plane_capacity_domain
        CHECK(Plane_Capacity > 0),
    -- Planes can't be serviced before they're built.
    CONSTRAINT plane_service_domain
        CHECK(TO_NUMBER(TO_CHAR(Last_Service, 'YYYY')) >= Year)
);

CREATE TABLE Flight(
    Flight_Number   VARCHAR(3),
    Airline_ID      VARCHAR(5),
    Plane_Type      CHAR(4),
    Departure_City  VARCHAR(3),
    Arrival_City    VARCHAR(3),
    Departure_Time  VARCHAR(4),
    Arrival_Time    VARCHAR(4),
    Weekly_Schedule VARCHAR(7),
    CONSTRAINT pk_flight
        PRIMARY KEY(Flight_Number)
        DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT fk_flight_plane_type
        FOREIGN KEY(Plane_Type, Airline_ID) 
        REFERENCES Plane(Plane_Type, Owner_ID)
        INITIALLY DEFERRED DEFERRABLE,
    CONSTRAINT fk_flight_airline_id
        FOREIGN KEY(Airline_ID) REFERENCES Airline(Airline_ID)
        INITIALLY DEFERRED DEFERRABLE,
    -- Planes cannot arrive before they depart.
    CONSTRAINT flight_arrival
        CHECK(Arrival_Time >= Departure_Time)
);

CREATE TABLE Price(
    Departure_City VARCHAR(3),
    Arrival_City   VARCHAR(3),
    Airline_ID     VARCHAR(5),
    High_Price     INTEGER,
    Low_Price      INTEGER,
    CONSTRAINT pk_price
        PRIMARY KEY(Departure_City, Arrival_City)
        DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT fk_price_airline_id
        FOREIGN KEY(Airline_ID) REFERENCES Airline(Airline_ID)
        INITIALLY DEFERRED DEFERRABLE,
    -- High price cannot be less than low price.
    CONSTRAINT price_domain
        CHECK(High_Price >= Low_Price)
);

CREATE TABLE Customer(
    CID                VARCHAR(9),
    Salutation         VARCHAR(3),
    First_Name         VARCHAR(30),
    Last_Name          VARCHAR(30),
    Credit_Card_Num    VARCHAR(16),
    Credit_Card_Expire DATE,
    Street             VARCHAR(30),
    City               VARCHAR(30),
    State              VARCHAR(2),
    Phone              VARCHAR(10),
    Email              VARCHAR(30),
    Frequent_Miles     VARCHAR(5) DEFAULT 0,
    CONSTRAINT pk_customer
        PRIMARY KEY(CID)
        DEFERRABLE INITIALLY IMMEDIATE
);

CREATE TABLE Reservation(
    Reservation_Number VARCHAR(5),
    CID                VARCHAR(9),
    Cost               INTEGER DEFAULT 0,    -- Total cost of trip.
    Credit_Card_Num    VARCHAR(16),
    Reservation_Date   DATE,       -- Date reservation was placed.
    Ticketed           VARCHAR(1), -- Y or N.
    Departure_City     VARCHAR(3), -- Start_City
    Arrival_City       VARCHAR(3), -- End_City
    CONSTRAINT pk_reservation
        PRIMARY KEY(Reservation_Number)
        DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT fk_reservation_cid
        FOREIGN KEY(CID) REFERENCES Customer(CID)
        INITIALLY DEFERRED DEFERRABLE,
    -- Tickets must be 'Y' or 'N'
    CONSTRAINT reservation_ticket_domain
        CHECK(Ticketed = 'Y' OR Ticketed = 'N'),
    -- Costs cannot be negative.
    CONSTRAINT reservation_cost_domain
        CHECK(Cost >= 0)
);

CREATE TABLE Reservation_Detail(
    Reservation_Number VARCHAR(5),
    Flight_Number      VARCHAR(3),
    Flight_Date        DATE, -- Date of flight.
    Leg                INTEGER,
    CONSTRAINT pk_res_det
        PRIMARY KEY (Reservation_Number, Leg)
        DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT fk_res_det_reservation_number
        FOREIGN KEY(Reservation_Number)
        REFERENCES Reservation(Reservation_Number),
    -- Leg cannot be negative.
    CONSTRAINT res_det_leg_domain
        CHECK(Leg >= 0 AND Leg < 4) -- Max of 4 legs per reservation
    -- Check that a flight actually exists at this time.
);

CREATE TABLE Date_Info(
    C_Date DATE, -- Current date?
    CONSTRAINT pk_date
        PRIMARY KEY(C_Date)
        DEFERRABLE INITIALLY IMMEDIATE
);

show errors;
COMMIT;
