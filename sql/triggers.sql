------------------------------TRIGGERS--------------------------------
SET TRANSACTION READ WRITE NAME 'Create triggers';
SET CONSTRAINTS ALL DEFERRED;

----------------------------adjustTicket------------------------------
--Adjusts the cost of a reservation when
--the price of one of its legs changes before
--the ticket is issued.
--
----------------------Approach-------------------------
-- When we update the price of a flight.
-- Grab all non-issued reservation details that have
--    the corresponding flight.
-- Determine whether we're using a
--    high or low price for each flight.
-- Calculate the difference for each flight.
-- Add the difference to the corresponding
--    reservation for each flight.

CREATE OR REPLACE TRIGGER adjustTicket
    BEFORE UPDATE
    ON Price
--  OF low or high price.
    FOR EACH ROW
DECLARE
    leg_price  INTEGER;
    flight_num VARCHAR(3);
BEGIN
    flight_num := fun_get_flight_number (:new.Arrival_City,
                                         :new.Departure_City);

    -- Select all non-issued reservations with the flight number
    FOR reservation_record IN (
        SELECT DISTINCT *
        FROM Reservation
        WHERE Arrival_City   = :new.Arrival_City
          AND Departure_City = :new.Departure_City)
    LOOP
        -- If we are getting a low price.
        IF fun_leg_price(reservation_record.Reservation_Number)
        THEN
            UPDATE Reservation    
            SET Cost = :new.Low_Price
            WHERE Reservation_Number = reservation_record.Reservation_Number;

        -- Else we are getting a high price.
        ELSE
            UPDATE Reservation    
            SET Cost = :new.High_Price
            WHERE Reservation_Number = reservation_record.Reservation_Number;
        END IF;
    END LOOP;
END;
/

----------------------------planeUpgrade------------------------------
--Changes the plane (type) to an immediately higher
--capacity plane (type), if it exists, when a new
--reservation is made on an already full flight.
--This should only fail if the plane is already
--of the biggest capacity.
--
-- Makes the assumption that the new plane
-- is owned by the same airline.
--
-- Makes the assumption that there will always
-- be an available plane.

CREATE OR REPLACE TRIGGER planeUpgrade
    BEFORE INSERT
    ON Reservation_Detail
    FOR EACH ROW

DECLARE
    current_capacity  INTEGER;
    capacity_of_plane INTEGER;
    current_plane     CHAR(4);
    new_plane         CHAR(4);
    air_id            VARCHAR(5);

BEGIN
    -- Get the current capacity.
    current_capacity := fun_get_passengers (:new.Flight_Number);

    -- Get the current plane type.
    SELECT Plane_Type, Airline_ID INTO current_plane, air_id
    FROM Flight
    WHERE Flight_Number = :new.Flight_Number;

    -- Get the Plane's capacity and airline.
    SELECT Plane_Capacity INTO capacity_of_plane
    FROM Plane
    WHERE Plane_Type = current_plane
      AND Owner_ID = air_id;

    -- Check if adding the plane will cause a problem.
    IF current_capacity + 1 > capacity_of_plane THEN
        -- Need to check if the function returns NULL.
        -- If so, rollback.
        new_plane := fun_get_bigger_plane (:new.Flight_Number);

        IF new_plane IS NULL THEN
            RAISE_APPLICATION_ERROR(
                -20001,
                'Flight is full and no larger planes could be found.',
                FALSE
            );

        -- Else change the plane.
        ELSE
            UPDATE Flight
            SET Plane_Type = new_plane
            WHERE Flight_Number = :new.Flight_Number;
        END IF;
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(
            -20000,
               'FNum: '      || :new.Flight_Number 
            || ', PType: '   || current_plane
            || ', Airline: ' || air_id,
            FALSE
        );
END;
/

show errors;

-------------------------cancelReservation----------------------------
-- Deletes all non-ticketed reservations for a flight,
-- 12 hours prior to the flight.
-- If the flight capacity fits in a smaller plane,
-- the plane should be switched.
--
-- Check every Reservation and if the
-- first flight happens within 12 hours
-- of the new date, cancel it.
--
-- Have a procedure to perform the plane update.

CREATE OR REPLACE TRIGGER cancelReservation
    BEFORE UPDATE
    ON Date_Info
    FOR EACH ROW

DECLARE

BEGIN
    FOR reservation_record IN (
        -- Grab the first leg 
        -- of reservations that 
        -- aren't ticketed and are
        -- within 12 hours.
        SELECT Reservation_Number
        FROM Non_Ticketed_Detail
        WHERE to_number (:new.C_Date - Flight_Date) * 24 <= 12
          AND Leg = 1)

    LOOP
        -- Delete the corresponding legs
        -- Then delete the corresponding reservation.
        -- For all deleted legs, check if a smaller
        -- plane can be found.
        FOR detail_record IN (
            SELECT *
            FROM Reservation_Detail
            WHERE Reservation_Number = 
                  reservation_record.Reservation_Number)

        LOOP
            -- Change to a smaller plane
            -- if it is necessary.
            proc_change_plane (detail_record.Flight_Number);
        END LOOP;

        -- Get rid of the reservation.
        delete from Reservation
        where Reservation_Number =
                reservation_record.Reservation_Number;
    END LOOP;
END;
/

show errors;
COMMIT;
