--------------------------------VIEWS---------------------------------
-- Join each Reservation with its details.
CREATE OR REPLACE VIEW Reservation_And_Detail AS
SELECT *
FROM Reservation NATURAL JOIN Reservation_Detail;

-- Join each flight with its price.
CREATE OR REPLACE VIEW Flight_And_Price AS
SELECT F.Flight_Number,   --  1
       F.Airline_ID,      --  2
       F.Plane_Type,      --  3
       F.Departure_City,  --  4
       F.Arrival_City,    --  5
       F.Departure_Time,  --  6
       F.Arrival_Time,    --  7
       F.Weekly_Schedule, --  8
       P.High_Price,      --  9
       P.Low_Price        -- 10
FROM Flight F
JOIN Price P
ON F.Departure_City = P.Departure_City
AND F.Arrival_City = P.Arrival_City;

-- Select only reservations that haven't been issued.
CREATE OR REPLACE VIEW Non_Ticketed_Detail AS
SELECT *
FROM Reservation_And_Detail
WHERE ticketed = 'N';

-- For getting flight capacities by flight number
CREATE OR REPLACE VIEW Flight_Capacity AS
SELECT F.Flight_Number, P.Plane_Capacity Capacity
FROM Plane P
JOIN Flight F ON F.Plane_Type = P.Plane_Type AND F.Airline_ID = P.Owner_ID;

-- For getting flight occupancies by flight number.
CREATE OR REPLACE VIEW Flight_Occupancy AS
SELECT Flight_Number, Flight_Date, COUNT(Reservation_Number) Reservations
FROM Reservation_Detail
GROUP BY Flight_Number, Flight_Date;
