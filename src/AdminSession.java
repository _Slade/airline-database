import java.sql.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.HashMap;

public class AdminSession implements Runnable {

    boolean DEBUG = false;

    Menu<AdminOption> menu = new Menu<AdminOption>(
        AdminOption.values(),
        System.console()
    );

    private final String username, password, url;

    public AdminSession(String username, String password, String url) {
        this.username = username;
        this.password = password;
        this.url      = url;
    }

    public enum AdminOption {
        ERASE
            ("(1) Erase the database"),
        LOAD_AIRLINE
            ("(2) Load airline information"),
        LOAD_SCHEDULE
            ("(3) Load schedule information"),
        LOAD_PRICES
            ("(4) Load pricing information"),
        LOAD_PLANES
            ("(5) Load plane information"),
        MAKE_MANIFEST
            ("(6) Generate passenger manifest for specific flight on given day"),
        SHOW_SUMMARY
            ("(7) Summarize database contents"),
        SET_SYSTIME
            ("(8) Set the system time"),
        LOAD_ALL
            ("(9) Load all data from default paths");
        private final String menu;
        AdminOption(String s) {
            menu = Util.requireNonNull(s);
        }
        public String toString() {
            return this.menu;
        }
    }

    public void run() {
        AdminOption opt;
        LOOP:
        while (true) {
            menu.draw();
            opt = menu.getOpt();
            if (opt == null) {
                continue;
            }
            switch (opt) {
                case ERASE:         erase();        break;
                case LOAD_AIRLINE:  loadAirline();  break;
                case LOAD_SCHEDULE: loadSchedule(); break;
                case LOAD_PRICES:   loadPrice();    break;
                case LOAD_PLANES:   loadPlane();    break;
                case MAKE_MANIFEST: makeManifest(); break;
                case SHOW_SUMMARY:  showSummary();  break;
                case SET_SYSTIME:   setSysTime();   break;
                case LOAD_ALL:      loadAll("sample_data"); break;
                default:
                    menu.out.println("Input not understood, please try again.");
                    break;
            }
        }
    }

    public void erase() {
        erase(menu.promptYesOrNo("Do you really want to delete all data?"));
    }

    /**
     * Truncates all tables in the database.
     *
     * @param confirmation Whether to truncate the tables.
     */
    public void erase(boolean confirmation) {
        if (!confirmation) {
            return;
        }

        Connection con = null;
        Statement stmt = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            String[] queries = {
                "SET TRANSACTION READ WRITE NAME 'Erase'",
                "SET CONSTRAINTS ALL DEFERRED",
                "DELETE FROM Reservation_Detail",
                "DELETE FROM Reservation",
                "DELETE FROM Customer",
                "DELETE FROM Date_Info",
                "DELETE FROM Price",
                "DELETE FROM Flight",
                "DELETE FROM Plane",
                "DELETE FROM Airline",
                "PURGE recyclebin",
                "COMMIT"
            };

            con.setAutoCommit(false);
            for (String query : queries) {
                stmt = con.createStatement();
                if (DEBUG) {
                    menu.out.println("Executing query: " + query);
                }
                stmt.executeQuery(query);
            }

            menu.out.println("Truncated all tables.");
        }
        catch (SQLException e) {
            menu.out.println("Error: " + e.getMessage());
            if (DEBUG) {
                e.printStackTrace(menu.out);
            }
        }
        finally {
            /* Note: Java 1.6 does not have try-with-resources, so this has to
                     be handled the old way. */
            Util.safeClose(con);
            Util.safeClose(stmt);
        }
    }

    /**
     * Load airline data from a file. The file should consist of a number of
     * tuples containing the Airline ID, Airline Name, Airline Abbreviation, and
     * Year Founded. The fields should be separated by comma characters.
     */
    public void loadAirline() {
        String filename = menu.prompt("Enter the path to the file");
        if (filename.length() == 0) {
            return;
        }
        menu.out.printf("Loaded %d airlines.%n", loadAirline(filename));
    }

    public int loadAirline(String filename) {
        int[] maxFieldLens = { 5, 50, 10, 10 };
        CSVLoader loader = new CSVLoader(maxFieldLens, menu.out) {
            @Override public int insertRow(
                Connection con,
                String[] fields
            ) throws Exception {
                PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO Airline VALUES(?, ?, ?, ?)"
                );
                stmt.setString(1, fields[0]); // ID
                stmt.setString(2, fields[1]); // Name
                stmt.setString(3, fields[2]); // Abbr
                stmt.setInt(4, Integer.parseInt(fields[3])); // Year
                return stmt.executeUpdate();
            }
        };
        return loader.load(filename, username, password, url);
    }

    public void loadSchedule() {
        String filename = menu.prompt("Enter the path to the file");
        if (filename.length() == 0) {
            return;
        }
        menu.out.printf(
            "Loaded information for %d flights.%n",
            loadSchedule(filename)
        );
    }

    public int loadSchedule(String filename) {
        int[] maxFieldLens = { 3, 5, 4, 3, 3, 4, 4, 7 };
        CSVLoader loader = new CSVLoader(maxFieldLens, menu.out) {
            @Override public int insertRow(
                Connection con,
                String[] fields
            ) throws Exception {
                PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO Flight VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
                );
                stmt.setInt(1, Integer.parseInt(fields[0]));
                for (int i = 1; i < 8; ++i) {
                    stmt.setString(i + 1, fields[i]);
                }
                return stmt.executeUpdate();
            }
        };
        return loader.load(filename, username, password, url);
    }

    public void loadPrice() {
        String resp = menu.prompt("[L]oad pricing information or [C]hange price");
        if ("C".equalsIgnoreCase(resp)) {
            changePrice();
            return;
        }
        else if ("L".equalsIgnoreCase(resp)) {
            String filename = menu.prompt("Enter the path to the file");
            if (filename.length() == 0) {
                return;
            }
            menu.out.printf(
                "Loaded pricing information for %d flights",
                loadPrice(filename)
            );

        }
        else {
            menu.out.println("Input not understood.");
        }
    }

    public int loadPrice(String filename) {
        int[] maxFieldLens = { 3, 3, 5, 10, 10 };
        CSVLoader loader = new CSVLoader(maxFieldLens, menu.out) {
            @Override public int insertRow(
                Connection con,
                String[] fields
            ) throws Exception {
                PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO Price VALUES(?, ?, ?, ?, ?)"
                );
                stmt.setString(1, fields[0]);
                stmt.setString(2, fields[1]);
                stmt.setString(3, fields[2]);
                stmt.setInt(4, Integer.parseInt(fields[3]));
                stmt.setInt(5, Integer.parseInt(fields[4]));
                try {
                    return stmt.executeUpdate();
                }
                catch (SQLException e) {
                    menu.out.println(
                        "Got an exception when inserting:"
                        + Util.join("|", fields)
                    );
                    if (e.getMessage().contains("FK_PRICE_AIRLINE_ID")) {
                        menu.out.println(
                            "The culprit airline ID is " + fields[2]
                        );
                    }
                    throw e;
                }
            }
        };
        return loader.load(filename, username, password, url);
    }

    /**
     * Return whether the given city code is a nonnull nonempty string with up
     * to 3 letters.
     */
    private boolean isValidCityCode(String city) {
        if (city == null) {
            return false;
        }
        if (city.length() > 3) {
            menu.out.println("City specifier must be no longer than 3 letters.");
            return false;
        }
        if (city.length() == 0) {
            menu.out.println("City specifier cannot be empty.");
            return false;
        }
        return true;
    }

    public void changePrice() {
        String deptCity = menu.prompt("Enter the departure city");
        if (!isValidCityCode(deptCity)) { return; }
        String arrvCity = menu.prompt("Enter the arrival city");
        if (!isValidCityCode(arrvCity)) { return; }
        Integer highPrice = menu.promptNum("high price");
        if (highPrice == null) { return; }
        Integer lowPrice  = menu.promptNum("low price");
        if (lowPrice == null) { return; }

        changePrice(deptCity, arrvCity, highPrice, lowPrice);
    }

    /**
     * Change the price of an existing flight. User must supply the departure
     * city, arrival city, high price and low price. Update the prices for the
     * flight specified by the departure city and arrival city that the user
     * enters.
     *
     * TODO: Make this inform the user if the row he's attempting to update
     * doesn't exist.
     */
    public void changePrice(
        String deptCity,
        String arrvCity,
        Integer highPrice,
        Integer lowPrice
    ) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.prepareStatement(
                  "UPDATE Price"
                + "SET    High_Price     = ?,"
                + "       Low_Price      = ?"
                + "WHERE  Departure_City = ?"
                + "AND    Arrival_City   = ?"
            );
            stmt.setInt(1, highPrice);
            stmt.setInt(2, lowPrice);
            stmt.setString(3, deptCity);
            stmt.setString(4, arrvCity);
            stmt.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace(); // TODO
        }
        finally {
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    public void loadPlane() {
        String filename = menu.prompt("Enter the path to the file");
        if (filename.length() == 0) {
            return;
        }
        menu.out.printf(
            "Loaded %d planes.%n",
            loadPlane(filename)
        );

    }

    public int loadPlane(String filename) {
        int[] maxFieldLens = { 4, 10, 10, 10, 10, 5 };
        CSVLoader loader = new CSVLoader(maxFieldLens, menu.out) {
            @Override public int insertRow(
                Connection con,
                String[] fields
            ) throws Exception {
                PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO Plane VALUES(?, ?, ?, ?, ?, ?)"
                );
                stmt.setString(1, fields[0]);                // Plane type
                stmt.setString(2, fields[1]);                // Manufacturer
                stmt.setInt(3, Integer.parseInt(fields[2])); // Capacity
                stmt.setDate(4, Util.parseDate(fields[3]));  // Last service
                stmt.setInt(5, Integer.parseInt(fields[4])); // Year
                stmt.setString(6, fields[5]);                // Owner ID
                return stmt.executeUpdate();
            }
        };
        return loader.load(filename, username, password, url);
    }

    /**
     * Prompt the user for a flight number and date, then show the salutation,
     * first name, and last name for each passenger with a ticket for that
     * flight.
     */
    public void makeManifest() {
        String flightNum = menu.prompt("Enter the flight number", 3);
        if (flightNum == null) {
            return;
        }
        Date flightDate = menu.promptDate("Enter the flight date");
        if (flightDate == null) {
            return;
        }

        makeManifest(flightNum, flightDate);
    }

    public void makeManifest(String flightNum, Date flightDate) {
        Connection        con  = null;
        PreparedStatement stmt = null;
        ResultSet         rs   = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.prepareStatement(
                  "SELECT C.Salutation, C.First_Name, C.Last_Name "
                + "FROM Customer C JOIN Reservation R ON C.CID = R.CID "
                + "JOIN Reservation_Detail RD "
                + "   ON R.Reservation_Number = RD.Reservation_Number "
                + "WHERE TO_NUMBER(RD.Flight_Number) = TO_NUMBER(?) "
                + "AND RD.Flight_Date = ? "
                + "AND R.Ticketed = 'Y'"
            );
            stmt.setString(1, flightNum);
            stmt.setDate(2, flightDate);
            rs = stmt.executeQuery();
            menu.out.println(
                "Manifest for flight " + flightNum
                + " on " + flightDate
            );
            menu.out.println(Util.repeatString("=", 80));
            while (rs.next()) {
                menu.out.printf(
                    "%s. %s %s%n",
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3)
                );
            }
        }
        catch (SQLException e) {
            menu.out.println("Error generating manifest: " + e.getMessage());
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    public void showSummary() {
        boolean r = menu.promptYesOrNo("Show sample rows from each table?");
        showSummary(r);
    }

    public void showSummary(boolean r) {
        String[] tables = {
            "Airline", "Customer", "Date_Info",   "Flight",
            "Plane",   "Price",    "Reservation", "Reservation_Detail"
        };
        Connection con = null;
        Statement stmt = null;

        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.createStatement();
            for (String table : tables) {
                String query = "SELECT COUNT(*) FROM " + table;
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    menu.out.printf("%-20s %5d rows%n", table + ":", rs.getInt(1));
                }
            }
            if (r) {
                for (String table : tables) {
                    String query = "SELECT * FROM " + table
                        + " WHERE ROWNUM <= 10";
                    ResultSet rs = stmt.executeQuery(query);
                    int cols = rs.getMetaData().getColumnCount();
                    menu.out.printf("%n%s:%n", table);
                    menu.out.println(Util.repeatString("=", 80));
                    while (rs.next()) {
                        for (int i = 1; i < cols; ++i) {
                            menu.out.print(rs.getString(i) + "|");
                        }
                        menu.out.println();
                    }
                }
            }
        }
        catch (SQLException e) {
            menu.out.println("Error querying DB: " + e.getMessage());
        }
        finally {
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    /** Prompt admin for a new system time and set it. */
    public void setSysTime() {
        Date d = menu.promptDate("Enter the new date");
        if (d == null) {
            return;
        }
        setSysTime(d, true);
    }

    /**
     * Set the system time to {@code d}.
     * @param d The new time.
     * @param overwrite Whether to overwrite the system time if it's already
     * set.
     */
    public void setSysTime(Date d, boolean overwrite) {
        Connection        con   = null;
        Statement         stmt1 = null;
        PreparedStatement stmt2 = null;
        ResultSet         rs    = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            con.setAutoCommit(false);
            stmt1 = con.createStatement();
            rs = stmt1.executeQuery("SELECT * FROM Date_Info");
            if (overwrite || !rs.next()) {
                stmt1.executeUpdate("DELETE FROM Date_Info");
                stmt2 = con.prepareStatement("INSERT INTO Date_Info VALUES(?)");
                stmt2.setDate(1, d);
                stmt2.executeUpdate();
                con.commit();
            }
        }
        catch (SQLException e) {
            try {
                if (con != null) {
                    con.rollback();
                }
            }
            catch (SQLException e2) {
                menu.out.println("Couldn't rollback: " + e2.getMessage());
            }
            menu.out.println("Error setting date: " + e.getMessage());
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(stmt1);
            Util.safeClose(stmt2);
            Util.safeClose(con);
        }
    }

    public void loadAll(String dataDir) {
        String[] searchPaths = { dataDir, "../" + dataDir };
        String[] defaults = {
            "airlines.csv",
            "planes.csv",
            "flights.csv",
            "prices.csv"
        };
        // File data directory
        Map<String, File> found = new HashMap<String, File>();
        for (String path : searchPaths) {
            File dir = new File(path);
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    found.put(f.getName(), f);
                }
                break;
            }
        }
        if (found.isEmpty()) {
            menu.out.println("Couldn't find data directory");
            return;
        }
        // Check that all files are there
        for (String s : defaults) {
            if (!found.containsKey(s)) {
                menu.out.println("Couldn't find file " + s);
                return;
            }
        }
        int airlines = loadAirline (found.get(defaults[0]).getPath());
        int planes   = loadPlane   (found.get(defaults[1]).getPath());
        int flights  = loadSchedule(found.get(defaults[2]).getPath());
        int prices   = loadPrice   (found.get(defaults[3]).getPath());
        menu.out.printf(
            "Loaded %d airlines, %d planes, %d flights, %d prices.%n",
            airlines,
            planes,
            flights,
            prices
        );
    }
}
