import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.io.*;

public class CSVLoader {
    private final int[] maxFieldLens;
    private final PrintWriter out;
    private static final String SQL_EXCEPTION_MESSAGE = Util.join(
        System.getProperty("line.separator"),
        "If this happened when you were attempting to load data, it likely means",
        "that your input was either invalid or that it relies on some foreign",
        "data that must be loaded first. Try loading in the following order:",
        "airlines, planes, flight schedules, prices."
    );

    public CSVLoader(int[] maxFieldLens, PrintWriter out) {
        this.maxFieldLens = maxFieldLens;
        this.out = out;
    }

    /** Load the CSV data and return the number of rows inserted. */
    public int load(
        String filename,
        String username,
        String password,
        String url
    ) {
        File file = new File(filename);
        LineNumberReader in = null;
        Connection con = null;
        int rowsInserted = 0;
        boolean committed = false;
        // Open a file reader
        try {
            in = new LineNumberReader(new FileReader(file));
        }
        catch (IOException e) {
            out.println("Error reading file: " + e.getMessage());
            return 0;
        }

        // Open a DB connection
        try {
            con = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException e) {
            out.println("Error opening DB connection: " + e.getMessage());
            Util.safeClose(in);
            return 0;
        }

        out.println("Attempting to load data...");
        try {
            while (in.ready()) {
                String[] fields = in.readLine().split(",");
                if (fields.length != maxFieldLens.length) {
                    out.printf(
                        "Wrong number of fields at %s line %d.%n",
                        filename,
                        in.getLineNumber()
                    );
                    break;
                }

                // Check field lengths. Should we just let the database do this?
                for (int i = 0; i < maxFieldLens.length; ++i) {
                    if (fields[i].length() > maxFieldLens[i]) {
                        out.printf(
                            "Field %d is too long at %s line %d.%n",
                            i, filename, in.getLineNumber()
                        );
                        break;
                    }
                }

                try {
                    rowsInserted += insertRow(con, fields);
                }
                catch (SQLException ex) {
                    out.printf("Error at %s line %d.%n", filename, in.getLineNumber());
                    out.println(SQL_EXCEPTION_MESSAGE);
                    out.println("The details of the error are as follows:");
                    for (Throwable e : ex) {
                        if (e instanceof SQLException) {
                            System.err.println(
                                "\tSQLState: " + ((SQLException)e).getSQLState()
                            );
                            System.err.println(
                                "\tError Code: " + ((SQLException)e).getErrorCode()
                            );
                            System.err.println(
                                "\tMessage: " + e.getMessage()
                            );
                            Throwable t = ex.getCause();
                            while (t != null) {
                                System.out.println("\tCause: " + t);
                                t = t.getCause();
                            }
                        }
                    }
                }
                catch (ParseException e) {
                    out.println("Error parsing date: " + e.getMessage());
                }
                catch (NumberFormatException e) {
                    out.printf(
                        "Error parsing integer at %s line %d.%n",
                        filename,
                        in.getLineNumber()
                    );
                }
                catch (Exception e) {
                    e.printStackTrace(out);
                }
            }
        }
        catch (IOException e) {
            out.println("Error reading file: " + e.getMessage());
        }
        finally {
            Util.safeClose(in);
            Util.safeClose(con);
        }

        return rowsInserted;
    }

    /**
     * This should be overridden. The override should create an INSERT
     * statement, set the parameters as necessary using the values of
     * {@code fields}, and execute the insertion.
     */
    public int insertRow(Connection con, String[] fields) throws Exception {
        return 0;
    }
}
