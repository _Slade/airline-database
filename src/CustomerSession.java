import java.text.ParseException;
import java.sql.*;
import java.io.*;

public class CustomerSession implements Runnable {

    boolean DEBUG = false;

    Menu<CustOption> menu = new Menu<CustOption>(
        CustOption.values(),
        System.console()
    );

    private final String username, password, url;

    public static final int MAX_LEGS = 4; // Maximum # of legs per reservation

    public CustomerSession(String username, String password, String url) {
        this.username = username;
        this.password = password;
        this.url      = url;
    }

    public enum CustOption {
        ADD_CUSTOMER
            ("(1) Add customer"),
        SHOW_CUSTOMER
            ("(2) Show customer info, given customer name"),
        FIND_PRICE
            ("(3) Find price for flights between two cities"),
        FIND_ROUTES
            ("(4) Find all routes between two cities"),
        FIND_AIRLINE_RTES
            ("(5) Find all routes between two cities of a given airline"),
        FIND_OPEN_RTES
            ( "(6) Find all routes with available seats between two cities on a"
            + " given day"),
        FIND_OPEN_AIRLINE_RTES
            ( "(7) For a given airline, find all routes with available seats"
            + " between two cities on given day" ),
        ADD_RESERVATION
            ("(8) Add reservation"),
        SHOW_RESERVATION
            ("(9) Show reservation info, given reservation number"),
        BUY_TICKET
            ("(10) Buy ticket from existing reservation");
        private final String menu;
        CustOption(String s) {
            menu = Util.requireNonNull(s);
        }
        public String toString() {
            return this.menu;
        }
    }

    public void run() {
        CustOption opt;
        LOOP:
        while (true) {
            menu.draw();
            opt = menu.getOpt();
            if (opt == null) {
                continue;
            }
            switch (opt) {
                case ADD_CUSTOMER:           addCustomer();            break;
                case SHOW_CUSTOMER:          showCustomer();           break;
                case FIND_PRICE:             findPrice();              break;
                case FIND_ROUTES:            findRoutes(false, false); break;
                case FIND_AIRLINE_RTES:      findRoutes(true, false);  break;
                case FIND_OPEN_RTES:         findRoutes(false, true);  break;
                case FIND_OPEN_AIRLINE_RTES: findRoutes(true, true);   break;
                case ADD_RESERVATION:        addReservation();         break;
                case SHOW_RESERVATION:       showReservation();        break;
                case BUY_TICKET:             buyTicket();              break;
            }
        }
    }

    /**
     * Normalize a phone number by stripping spaces, hyphens, leading plus
     * signs. If the phone number is invalid (e.g. contains letters), return
     * {@code null}.
     */
    public String normalizePhoneNumber(String s) {
        if (!s.matches("^\\+?[- \\d]+$")) {
            return null;
        }
        else {
            return s.replaceAll("\\D", "");
        }
    }

    /**
     * Normalize a credit card number by stripping spaces and hyphens. If the
     * phone number is invalid (e.g. contains letters), return {@code null}.
     */
    public String normalizeCCNumber(String s) {
        if (!s.matches("^[- \\d]+$")) {
            return null;
        }
        else {
            return s.replaceAll("\\D", "");
        }
    }

    /** Trim a trailing '.' from a string if it exists. */
    public static String trimEndingPeriod(String s) {
        if (s.endsWith(".")) {
            s = s.substring(0, Util.max(s.length() - 1, 0));
        }
        return s;
    }

    public void addCustomer() {
        menu.out.println(
            Util.join(
                System.getProperty("line.separator"),
                "Please enter your salutation (Mr/Mrs/Ms), first name, last",
                "name, address (street, city, state), phone number (without",
                "hyphens), email address, credit card number (without hyphens),",
                "and credit card expiration date (in MM/DD/YYYY format).",
                "Empty input returns to the main menu."
            )
        );
        String[] prompts = {
            "Salutation",    // 0
            "First name",    // 1
            "Last name",     // 2
            "Street",        // 3
            "City",          // 4
            "State",         // 5
            "Phone number",  // 6
            "Email address", // 7
            "CC number",     // 8
        };
        int[] maxLengths = {
            3,  // Salutation
            30, // First_Name
            30, // Last_Name
            30, // Street
            30, // City
            2,  // State
            10, // Phone
            30, // Email
            16, // Credit_Card_Num
        };
        String[] fields = new String[prompts.length];
        for (int i = 0; i < prompts.length; ++i) {
            RETRY:
            while (true) {
                fields[i] = menu.prompt(prompts[i]);
                if (fields[i].length() == 0) {
                    return;
                }
                else if (fields[i].length() > maxLengths[i]) {
                    menu.out.println(
                          prompts[i]    + " cannot be longer than "
                        + maxLengths[i] + " characters, please try again."
                    );
                    continue RETRY;
                }
                else if (i == 0) {
                    fields[i] = trimEndingPeriod(fields[i]);
                    if (fields[i].length() == 0) {
                        menu.out.println("Invalid salutation, please try again.");
                        continue RETRY;
                    }
                }
                else if (i == 6) {
                    fields[i] = normalizePhoneNumber(fields[i]);
                    if (fields[i] == null) {
                        menu.out.println(
                              "Phone number contains illegal characters,"
                            + " please re-enter."
                        );
                        continue RETRY;
                    }
                }
                else if (i == 8) { // Special case for checking CC number
                    fields[i] = normalizeCCNumber(fields[i]);
                    if (fields[i] == null) {
                        menu.out.println(
                            "Credit card number contains illegal characters,"
                            + " please re-enter."
                        );
                        continue RETRY;
                    }
                }
                break RETRY;
            }
        }

        Date ccExpireDate = menu.promptDate("CC expiration date");
        if (ccExpireDate == null) {
            return;
        }
        menu.out.println("Verifying credit card...");
        Date currentDate = null;
        try {
            currentDate = Util.getSystemDate(username, password, url);
        }
        catch (SQLException e) { }
        if (currentDate == null) {
            menu.out.println(
                "Couldn't get current date for comparison,"
                + " assuming the CC is not expired."
            );
        }
        else if (ccExpireDate.getTime() < currentDate.getTime()) {
            menu.out.println("Your credit card appears to be expired.");
            if (!menu.promptYesOrNo("Use it anyway?")) {
                return; // Back out due to bad CC
            }
        }
        else {
            menu.out.println("Credit card looks OK.");
        }

        addCustomer(fields, ccExpireDate);
    }

    public void addCustomer(String[] fields, Date ccExpireDate) {
        Connection con = null;
        Statement getCID = null;
        PreparedStatement stmt = null;
        try {
            // Connect
            con = DriverManager.getConnection(url, username, password);

            // Get a new CID
            getCID = con.createStatement();
            ResultSet rs = getCID.executeQuery("SELECT MAX(CID) FROM Customer");
            int CID;
            if (rs.next()) {
                CID = rs.getInt(1) + 1;
            }
            else {
                menu.out.println(
                      "Error assigning ID to customer:"
                    + " Couldn't find max CID to increment"
                );
                return;
            }

            // Create insertion statement
            stmt = con.prepareStatement(
                String.format(
                    "INSERT INTO Customer(%s) VALUES (%s)",
                    Util.join(", ",
                        "Salutation",         // 1
                        "First_Name",         // 2
                        "Last_Name",          // 3
                        "Street",             // 4
                        "City",               // 5
                        "State",              // 6
                        "Phone",              // 7
                        "Email",              // 8
                        "Credit_Card_Num",    // 9
                        "Credit_Card_Expire", // 10
                        "CID",                // 11
                        "Frequent_Miles"      // 12
                    ),
                    Util.join(", ", Util.repeatList((CharSequence)"?", 12))
                )
            );
            // Insert all string fields (not CID, Frequent_Miles, or CC date)
            for (int i = 0; i < fields.length; ++i) {
                stmt.setString(i + 1, fields[i]);
            }
            stmt.setDate(10, ccExpireDate);
            stmt.setInt(11, CID);
            stmt.setInt(12, 0); // New users have no frequent miles
            stmt.executeUpdate();
            menu.out.println("Your PittRewards number (CID) is " + CID);
        }
        catch (SQLException e) {
            menu.out.println("Error inserting new customer: " + e.getMessage());
        }
        finally {
            Util.safeClose(stmt);
            Util.safeClose(getCID);
            Util.safeClose(con);
        }
    }

    /**
     * Prompt the user for a customer's name (first and last), then print the
     * information for the user(s) with that name. Note that we do not specify
     * {@code UNIQUE(First_Name, Last_Name)}, so we paginate results.
     */
    public void showCustomer() {
        String firstName = menu.prompt("Enter the customer's first name");
        if (firstName.length() == 0) {
            return;
        }
        String lastName  = menu.prompt("Enter the customer's last name");
        if (lastName.length() == 0) {
            return;
        }

        showCustomer(firstName, lastName);
    }

    public void showCustomer(String firstName, String lastName) {
        Connection con         = null;
        PreparedStatement stmt = null;
        ResultSet rs           = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.prepareStatement(
                "SELECT * FROM Customer WHERE First_Name = ? AND Last_Name = ?"
            );
            stmt.setString(1, firstName);
            stmt.setString(2, lastName);
            boolean isFirst = true;
            rs = stmt.executeQuery();
            while (rs.next()) {
                if (isFirst) {
                    isFirst = false;
                }
                else if (!menu.promptYesOrNo("Show next result?")) {
                    break;
                }
                // Print the user's information
                String[] cols = new String[12];
                for (int j = 0; j < cols.length; ++j) {
                    cols[j] = rs.getString(j + 1);
                }
                menu.out.printf(
                      "%n"
                    + "CID            : %-9s%n"
                    + "Name           : %s. %s %s%n"
                    + "Credit card    : %s (expires %s)%n"
                    + "Address        : %s, %s, %s%n"
                    + "Phone number   : %s%n"
                    + "Email address  : %s%n"
                    + "Frequent miles : %s%n",
                    (Object[])cols
                );
            }
        }
        catch (SQLException e) {
            menu.out.println("Error retrieving customer info: " + e.getMessage());
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    /**
     * Prompt the user for two cities, A and B, and then
     * print out the corresponding tickets for a trip from
     * A to B and B to A, and a round trip from A to B.
     */
    public void findPrice() {
        String cityA = menu.prompt("Enter city A");
        if (cityA.length() == 0) {
            return;
        }
        String cityB = menu.prompt("Enter city B");
        if (cityB.length() == 0) {
            return;
        }
        menu.out.println(); // Put a gap between prompts and output

        findPrice(cityA, cityB);
    }

    public void findPrice(String cityA, String cityB) {
        /* Grab all of the possible flights from A to B, and
         * then print them out.
         * Grab all of the possible flights from B to A, and
         * then print them out.
         * Print out all combinations of tickets from A to B
         * to A. */

        Connection con             = null;
        PreparedStatement stmtAToB = null;
        PreparedStatement stmtBToA = null;
        ResultSet rsAToB           = null;
        ResultSet rsBToA           = null;
        boolean roundTripPossible  = true;
        try {
            con = DriverManager.getConnection(url, username, password);

            /* Grab the result of A to B. */
            stmtAToB = con.prepareStatement(
                "SELECT * "
              + "FROM Flight_And_Price "
              + "WHERE Departure_City = ? "
              + "AND   Arrival_City   = ?",
              ResultSet.TYPE_SCROLL_INSENSITIVE,
              ResultSet.CONCUR_READ_ONLY
            );
            stmtAToB.setString(1, cityA);
            stmtAToB.setString(2, cityB);
            rsAToB = stmtAToB.executeQuery();

            /* Inform the user if no flights
             * have been found from A to B.
             * Mark that a round trip isn't
             * possible now. */
            if (!rsAToB.next()) {
                menu.out.println(
                    "No flights from "
                    + cityA
                    + " to "
                    + cityB
                    + " found."
                );
                roundTripPossible = false;
            } else {
                menu.out.println(
                    "Flights from "
                    + cityA
                    + " to "
                    + cityB
                    + ":"
                );
                /* Print out all the flights. */
                do {
                    menu.out.printf(
                        "Flight: %s   High: %s  Low: %s%n",
                        rsAToB.getString(1),
                        rsAToB.getString(9),
                        rsAToB.getString(10)
                    );
                } while (rsAToB.next());
            }

            /* Grab the result of B to A. */
            stmtBToA = con.prepareStatement(
                "SELECT * "
              + "FROM Flight_And_Price "
              + "WHERE Departure_City = ? "
              + "AND   Arrival_City   = ?",
              ResultSet.TYPE_SCROLL_INSENSITIVE,
              ResultSet.CONCUR_READ_ONLY
            );
            stmtBToA.setString(1, cityB);
            stmtBToA.setString(2, cityA);
            rsBToA = stmtBToA.executeQuery();

            if (!rsBToA.next()) {
                menu.out.println(
                    "No flights from "
                    + cityB
                    + " to "
                    + cityA
                    + " found."
                );
                roundTripPossible = false;
            } else {
                menu.out.println(
                    "Flights from "
                    + cityB
                    + " to "
                    + cityA
                    + ":"
                );

                /* Print out all the flights. */
                do {
                    menu.out.printf(
                        "Flight: %s   High: %s  Low: %s%n",
                        rsBToA.getString(1),
                        rsBToA.getString(9),
                        rsBToA.getString(10)
                    );
                } while (rsAToB.next());
            }

            if (roundTripPossible) {
                menu.out.println("Round trips:");
                /* pull back to the zeroeth row. */
                rsAToB.beforeFirst();

                /* Go through every possible
                 * combination, printing out their
                 * price. */
                while (rsAToB.next()) {
                    rsBToA.beforeFirst();

                    while (rsBToA.next()) {
                        menu.out.printf(
                            "Flights: %s AND %s%n    High: %d  Low: %d%n",
                            rsAToB.getString(1),
                            rsBToA.getString(1),
                            Integer.parseInt(rsAToB.getString(9))
                            + Integer.parseInt(rsBToA.getString(9)),
                            Integer.parseInt(rsAToB.getString(10))
                            + Integer.parseInt(rsBToA.getString(10))
                        );
                    }
                }

            } else {
                menu.out.println("No round trips possible.");
            }
        }
        catch (SQLException e) {
            menu.out.println("Error finding flights: " + e.getMessage());
        }
        catch (NumberFormatException e) {
            menu.out.println("Error parsing price: " + e.getMessage());
        }
        finally {
            Util.safeClose(rsAToB);
            Util.safeClose(rsBToA);
            Util.safeClose(stmtAToB);
            Util.safeClose(stmtBToA);
            Util.safeClose(con);
        }
    }

    /** Gets an airline's ID from its name. */
    private String getAirlineIDFromName(String airlineName) {
        Connection        con  = null;
        PreparedStatement stmt = null;
        ResultSet         rs   = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.prepareStatement(
                  "SELECT Airline_ID "
                + "FROM Airline "
                + "WHERE Airline_Name = ? "
            );
            stmt.setString(1, airlineName);
            rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        }
        catch (SQLException e) {
            if (DEBUG) {
                e.printStackTrace(menu.out);
            }
            /* Suppresses exception and returns null instead. */
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
        return null;
    }

    /**
     * Find and print routes.
     * @param con An open DB connection.
     * @param stmt A statement that retrieves all routes to be displayed.
     * @param formatString A format string for printing the results.
     * @param indices The indices of each field in the result, i.e. which column
     *                of the query contains the field's value. 1-indexed.
     */
    public void displayRoutes(Connection con, PreparedStatement stmt)
        throws SQLException {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery();
            final int expectedCols = 4;
            final int cols = rs.getMetaData().getColumnCount();
            assert cols % expectedCols == 0
                : "Unexpected number of columns in result set";
            while (rs.next()) {
                for (int i = 0; i < cols / expectedCols; ++i) {
                    menu.out.printf(
                        "%d : Flight number %3s, departure city: %3s,"
                        + " departure time: %4s, arrival time: %4s%n",
                        i + 1,
                        rs.getString((i * expectedCols) + 1),
                        rs.getString((i * expectedCols) + 2),
                        rs.getString((i * expectedCols) + 3),
                        rs.getString((i * expectedCols) + 4)
                    );
                }
                menu.out.println();
            }
        }
        finally {
            Util.safeClose(rs);
        }
    }

    public void findRoutes(boolean forAirline, boolean open) {
        String cityA = menu.prompt("Enter city A");
        if (cityA.length() == 0) {
            return;
        }
        String cityB = menu.prompt("Enter city B");
        if (cityB.length() == 0) {
            return;
        }
        String airline = null;
        if (forAirline) {
            String airlineName = menu.prompt("Enter an airline");
            if (airlineName.length() == 0) {
                return;
            }
            airline = getAirlineIDFromName(airlineName);
            if (airline == null) {
                menu.out.println(
                    "Error: No such airline \"" + airlineName + "\"."
                );
                return;
            }
        }
        Date date = null;
        if (open) {
            date = menu.promptDate("Enter the date");
            if (date == null) {
                return;
            }
        }

        findRoutes(forAirline, open, cityA, cityB, airline, date);
    }

    public void findRoutes(
        boolean forAirline,
        boolean open,
        String cityA,
        String cityB,
        String airline,
        Date date
    ) {
        if (cityA.equals(cityB)) {
            menu.out.println("Cannot fly in a loop.");
            return;
        }
        String directQuery = Util.joinLines(
            "SELECT Flight_Number, ",
            "       Departure_City, ",
            "       Departure_Time, ",
            "       Arrival_Time ",
            "FROM Flight A ",
            "WHERE Departure_City = ? ",
            "AND   Arrival_City   = ? "
        );

        String connectedQuery = Util.joinLines(
            "SELECT A.Flight_Number, ",
            "       A.Departure_City, ",
            "       A.Departure_Time, ",
            "       A.Arrival_Time, ",
            "       B.Flight_Number, ",
            "       B.Departure_City, ",
            "       B.Departure_Time, ",
            "       B.Arrival_Time ",
            "FROM Flight A, Flight B ",
            "WHERE A.Departure_City = ? ",
            "AND A.Arrival_City   = B.Departure_City ",
            "AND B.Arrival_City   = ? "
        );

        if (open) {
            final String CHECK_CAPACITY = Util.joinLines(
                "AND (",
                "   SELECT COALESCE(",
                "       (SELECT Reservations",
                "        FROM Flight_Occupancy FO",
                "        WHERE FO.Flight_Number = %1$s.Flight_Number",
                "        AND FO.Flight_Date = ?),",
                "   0) Occupancy",
                "   FROM DUAL",
                ") < ( ",
                "  SELECT Capacity ",
                "  FROM Flight_Capacity FC ",
                "  WHERE FC.Flight_Number = %1$s.Flight_Number ",
                ") "
            );
            directQuery    += Util.NL + String.format(CHECK_CAPACITY, "A");
            connectedQuery += Util.NL + String.format(CHECK_CAPACITY, "A");
            connectedQuery += Util.NL + String.format(CHECK_CAPACITY, "B");
        }
        else {
            connectedQuery += Util.NL + Util.joinLines(
                "AND (TO_NUMBER(B.Departure_Time) ",
                "   - TO_NUMBER(A.Departure_Time)) * 24 >= 1 "
            );
        }

        if (forAirline) {
            directQuery    += Util.NL + "AND A.Airline_ID = ? ";
            connectedQuery += Util.NL + Util.joinLines(
                "AND A.Airline_ID = ?",
                "AND A.Airline_ID = B.Airline_ID"
            );
        }

        if (DEBUG) {
            menu.out.printf(
                "Direct query (airline-specific = %b,"
                + " available seats = %b):%n%s%n%n",
                forAirline,
                open,
                directQuery
            );
            menu.out.printf(
                "Connected query (airline-specific = %b,"
                + " available seats = %b):%n%s%n%n",
                forAirline,
                open,
                connectedQuery
            );
        }

        Connection        con       = null;
        PreparedStatement direct    = null;
        PreparedStatement connected = null;
        try {
            con = DriverManager.getConnection(url, username, password);

            /* Grab the result of A to B. */
            int i = 1;
            direct = con.prepareStatement(directQuery);
            if (DEBUG) { menu.out.println("Binding direct parameters"); }
            // All queries need departure city, arrival city as first two
            // parameters
            direct.setString(i++, cityA);
            if (DEBUG) { menu.out.println("Bound " + cityA); }
            direct.setString(i++, cityB);
            if (DEBUG) { menu.out.println("Bound " + cityB); }
            if (open) {
                // Need date parameter if we're looking for open flights
                direct.setDate(i++, date);
                if (DEBUG) { menu.out.println("Bound " + date); }
            }
            if (forAirline) {
                // Need airline parameter if this is for a specific airline
                direct.setString(i++, airline);
                if (DEBUG) { menu.out.println("Bound " + airline); }
            }
            menu.out.println("Direct routes:");
            displayRoutes(con, direct);

            /* Grab the flights with connections. */
            i = 1;
            if (DEBUG) { menu.out.println("Binding connected parameters"); }
            connected = con.prepareStatement(connectedQuery);
            connected.setString(i++, cityA);
            if (DEBUG) { menu.out.println("Bound " + cityA); }
            connected.setString(i++, cityB);
            if (DEBUG) { menu.out.println("Bound " + cityB); }
            if (open) {
                // Need to bind two date parameters, one for each flight.
                // They should have the same value.
                connected.setDate(i++, date);
                if (DEBUG) { menu.out.println("Bound " + date); }
                connected.setDate(i++, date);
                if (DEBUG) { menu.out.println("Bound " + date); }
            }
            if (forAirline) {
                connected.setString(i++, airline);
                if (DEBUG) { menu.out.println("Bound " + airline); }
            }
            menu.out.println("Singly-connected routes:");
            displayRoutes(con, connected);
        }
        catch (SQLException e) {
            menu.out.println("Error: " + e.getMessage());
            if (DEBUG) {
                e.printStackTrace(menu.out);
            }
        }
    }

    public void addReservation() {
        // 1. Get customer's CID
        Integer CID = menu.promptNum("Enter your PittRewards number (CID)");
        if (CID == null) {
            return;
        }

        addReservation(CID);
    }

    public void addReservation(Integer CID) {
        Connection        con          = null;
        Statement         stmt         = null;
        PreparedStatement getCC        = null;
        PreparedStatement insertR      = null;
        PreparedStatement insertRD     = null;
        PreparedStatement getFlight    = null;
        Statement         getResNum    = null;
        ResultSet         rs           = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            // 2. Get customer's CC
            String CC = null;
            getCC = con.prepareStatement(
                "SELECT Credit_Card_Num FROM Customer WHERE CID = ?"
            );
            getCC.setInt(1, CID);
            rs = getCC.executeQuery();
            if (rs.next()) {
                CC = rs.getString(1);
            }
            else {
                menu.out.println("User " + CID + " does not exist.");
                return;
            }

            // 3. Get a new reservation number, or start at 1 if this is the
            // first one to be added.
            int resNum = 1;
            getResNum = con.createStatement();
            rs = getResNum.executeQuery(
                "SELECT MAX(TO_NUMBER(Reservation_Number)) FROM Reservation"
            );
            if (rs.next()) {
                resNum = rs.getInt(1) + 1;
            }

            // 4. Get flight numbers for up to 4 legs.
            Integer[] legs = new Integer[MAX_LEGS];
            Date[] legDates = new Date[MAX_LEGS];
            String firstDept = null,
                   lastArrv  = null;
            getFlight = con.prepareStatement(
                  "SELECT Departure_City, Arrival_City"
                + " FROM   Flight"
                + " WHERE  Flight_Number = ?"
            );
            GET_LEGS:
            for (int i = 0; i < MAX_LEGS; /* Don't autoincrement */) {
                Integer fNum = menu.promptNum(
                    "Enter the flight number of leg " + (i + 1)
                );
                if (fNum == null) {
                    if (i == 0) {
                        menu.out.println("No flights given, returning.");
                        return;
                    }
                    else {
                        break; // User is done entering flights.
                    }
                }
                // Ensure the endpoints match up.
                getFlight.clearParameters();
                getFlight.setInt(1, fNum);
                rs = getFlight.executeQuery();
                String arrv, dept;
                if (rs.next()) {
                    dept = rs.getString(1);
                    arrv = rs.getString(2);
                    if (i == 0) {
                        firstDept = dept; // Need this for the reservation
                    }
                }
                else {
                    menu.out.println(
                        "Flight " + fNum + " does not exist, please try again."
                    );
                    continue GET_LEGS;
                }
                if (lastArrv == null || lastArrv.equals(dept)) {
                    // Get the date for this flight and make sure it isn't full
                    Date fDate = menu.promptDate("Enter the date of this leg");
                    if (fDate == null) {
                        continue GET_LEGS; // Retry this leg
                    }

                    legs[i] = fNum; // Add this flight and its date to the legs
                    legDates[i] = fDate;
                    i++;
                    lastArrv = arrv;
                }
                else {
                    menu.out.printf(
                        "Departure city %s does not match last arrival city %s.%n",
                        dept,
                        lastArrv
                    );
                    continue GET_LEGS; // i stays the same in next iteration
                }
            }

            // Get current date
            Date currentDate = Util.getSystemDate(con);

            // Insert the reservation rows into the DB
            insertR = con.prepareStatement(
                String.format(
                    "INSERT INTO Reservation(%s) VALUES(%s)",
                    Util.join(", ",
                        "Reservation_Number", // resNum
                        "CID",                // CID
                        "Credit_Card_Num",    // CC
                        "Reservation_Date",   // currentDate
                        "Ticketed",           // "N"
                        "Departure_City",     // firstDept
                        "Arrival_City"        // lastArrv
                    ),
                    Util.join(", ", Util.repeatList((CharSequence)"?", 7))
                )
            );
            insertRD = con.prepareStatement(
                String.format(
                    "INSERT INTO Reservation_Detail(%s) VALUES(%S)",
                    Util.join(", ",
                        "Reservation_Number", // resNum
                        "Flight_Number",      // legs[...]
                        "Flight_Date",        // legDates[...]
                        "Leg"                 // 0, 1, 2, 3
                    ),
                    Util.join(", ", Util.repeatList((CharSequence)"?", 4))
                )
            );

            menu.out.println("Adding reservation...");
            try {
                con.setAutoCommit(false);
                // Handle concurrent insertions
                stmt = con.createStatement();
                stmt.execute(
                    "SET TRANSACTION ISOLATION LEVEL READ COMMITTED"
                    + " NAME 'Insert reservation'"
                );

                insertR.setInt(1, resNum);
                insertR.setInt(2, CID);
                insertR.setString(3, CC);
                insertR.setDate(4, currentDate);
                insertR.setString(5, "N");
                insertR.setString(6, firstDept);
                insertR.setString(7, lastArrv);
                insertR.executeUpdate();

                menu.out.println("Adding legs...");
                for (int i = 0; i < legs.length && legs[i] != null; ++i) {
                    assert legDates[i] != null;
                    insertRD.clearParameters();
                    insertRD.setInt(1, resNum);
                    insertRD.setInt(2, legs[i]);
                    insertRD.setDate(3, legDates[i]);
                    insertRD.setInt(4, i);
                    insertRD.executeUpdate();
                }

                con.commit();
                menu.out.println("Reservation made.");
                menu.out.println("Your reservation number is " + resNum + ".");
            }
            catch (SQLException e) {
                con.rollback();
                throw e;
            }
        }
        catch (SQLException e) {
            switch (e.getErrorCode()) {
                case 20000:
                    menu.out.println("Error: Flight or plane not found.");
                    break;
                case 20001:
                    menu.out.println(
                        "Error: Flight is full and no larger planes"
                        + " could be found."
                    );
                    break;
                default:
                    menu.out.println("Error: " + e.getMessage());
                    break;
            }
            if (DEBUG) {
                e.printStackTrace(menu.out);
            }
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(getCC);
            Util.safeClose(getFlight);
            Util.safeClose(getResNum);
            Util.safeClose(insertR);
            Util.safeClose(insertRD);
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    public void loadSQL(File file) throws IOException {
        LineNumberReader in = null;
        Connection con = null;
        Statement stmt = null;
        int rowsInserted = 0;
        int lines;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.createStatement();
            in = new LineNumberReader(new FileReader(file));
            while (in.ready()) {
                String line = in.readLine();
                line = line.replaceFirst(";\\s*$", "");
                rowsInserted += stmt.executeUpdate(line);
            }
        }
        catch (SQLException e) {
            menu.out.printf(
                "Error loading at line %d: %s%n",
                in.getLineNumber(),
                e.getMessage()
            );
        }
        finally {
            Util.safeClose(con);
            Util.safeClose(stmt);
            lines = in.getLineNumber();
            in.close();
        }
        menu.out.printf("Loaded %d rows from %d lines%n", rowsInserted, lines);
    }

    /**
     * Prompt the user for a reservation number, and then
     * print out the corresponding reservation if it exists.
     */
    public void showReservation() {
        String reservationNumber = menu.prompt("Enter the reservation number");
        showReservation(reservationNumber);
    }

    public void showReservation(String reservationNumber) {
        Connection con           = null;
        PreparedStatement stmt   = null;
        ResultSet rs             = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.prepareStatement(
                 "SELECT * FROM Reservation "
               + "WHERE TO_NUMBER(Reservation_Number) = TO_NUMBER(?)"
            );
            stmt.setString(1, reservationNumber);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new SQLException("Reservation not found");
            }

            // Print the reservation's information
            String[] cols = new String[8];
            for (int j = 0; j < cols.length; ++j) {
                cols[j] = rs.getString(j + 1);
            }
            menu.out.printf(
                  "%n"
                + "Reservation Number : %s%n"
                + "CID                : %s%n"
                + "Cost               : %s%n"
                + "Credit card        : %s%n"
                + "Reservation Date   : %s%n"
                + "Ticket Purchased   : %s%n"
                + "Departure City     : %s%n"
                + "Arrival City       : %s%n",
                (Object[])cols
            );
        }
        catch (SQLException e) {
            menu.out.println("Error retrieving reservation: " + e.getMessage());
        }
        finally {
            Util.safeClose(rs);
            Util.safeClose(stmt);
            Util.safeClose(con);
        }
    }

    public void verifyReservationCC(
        int resNum,
        Connection con
    ) throws SQLException, InvalidCreditCardException {
        // Get credit card
        PreparedStatement getCC    = null,
                          getCCExp = null;
        ResultSet         rs       = null;
        getCC = con.prepareStatement(
              "SELECT Credit_Card_Num, CID "
            + "FROM Reservation "
            + "WHERE TO_NUMBER(Reservation_Number) = ? "
            + "AND Credit_Card_Num IS NOT NULL"
        );
        getCC.setInt(1, resNum);
        rs = getCC.executeQuery();
        if (rs.next()) {
            String CC  = rs.getString(1);
            String CID = rs.getString(2);
            // Get credit card expiration date
            getCCExp = con.prepareStatement(
                  "SELECT Credit_Card_Expire "
                + "FROM Customer "
                + "WHERE Credit_Card_Num = ? "
                + "AND Credit_Card_Expire IS NOT NULL "
                + "AND CID = ?"
            );
            getCCExp.setString(1, CC);
            getCCExp.setString(2, CID);
            rs = getCCExp.executeQuery();
            if (rs.next()) {
                Date ccExpDate = rs.getDate(1);
                Date curDate = Util.getSystemDate(con);
                if (curDate == null) {
                    throw new InvalidCreditCardException(
                        "Could not get current (system) date."
                    );
                }
                else if (curDate.after(ccExpDate)) {
                    throw new InvalidCreditCardException(
                        "Credit card is expired."
                    );
                }
                // Otherwise it's good
            }
            else {
                throw new InvalidCreditCardException(
                    "Credit card does not have an associated"
                    + " expiration date, cannot verify purchase."
                );
            }
        }
        else {
            throw new InvalidCreditCardException(
                "Reservation does not have an associated"
                + " credit card, cannot purchase ticket."
            );
        }

        Util.safeClose(rs);
        Util.safeClose(getCC);
        Util.safeClose(getCCExp);
    }

    /**
     * Prompt the user for a reservation number, and then
     * update the corresponding reservation to be 'bought'.
     * Indicate to the user if the transaction was successful.
     * Requires an unexpired credit card to be associated with the reservation.
     * Assumes that multiple tickets may be purchased for a single reservation,
     * e.g. if a family is traveling together.
     */
    public void buyTicket() {
        /* Possibly check if the ticket has already
         * been bought. */
        Integer resNum = menu.promptNum("Enter the reservation number");
        if (resNum == null) {
            return;
        }

        buyTicket(resNum);
    }

    public void buyTicket(Integer resNum) {
        Connection        con = null;
        PreparedStatement buy = null;

        try {
            con = DriverManager.getConnection(url, username, password);

            verifyReservationCC(resNum, con);

            buy = con.prepareStatement(
                  "UPDATE Reservation "
                + "SET Ticketed = 'Y' "
                + "WHERE TO_NUMBER(Reservation_Number) = ?"
            );
            buy.setInt(1, resNum);
            buy.executeUpdate();
            con.commit();
            menu.out.println("Ticket has been bought.");
        }
        catch (SQLException e) {
            menu.out.println("Error buying ticket: " + e.getMessage());
        }
        catch (InvalidCreditCardException e) { // Java 6 doesn't allow |
            menu.out.println("Error buying ticket: " + e.getMessage());
        }
        finally {
            Util.safeClose(buy);
            Util.safeClose(con);
        }
    }

    public static void main(String[] args) {
        CustomerSession cs = new CustomerSession(null, null, null);
        cs.DEBUG = true;
        cs.findRoutes(false, false);
        cs.findRoutes(true, false);
        cs.findRoutes(false, true);
        cs.findRoutes(true, true);
    }
}
