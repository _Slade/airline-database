public class InvalidCreditCardException extends Exception {
    static final long serialVersionUID = -3704872938443556048L;

    public InvalidCreditCardException () { }

    public InvalidCreditCardException (String message) {
        super(message);
    }

    public InvalidCreditCardException (Throwable cause) {
        super(cause);
    }

    public InvalidCreditCardException (String message, Throwable cause) {
        super(message, cause);
    }
}
