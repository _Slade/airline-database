/**
 * Main class. Displays the user interface and calls the appropriate methods to
 * handle each action executed by the user.
 *
 * To enable all debugging messages, set the "debug" system property. To enable
 * debugging messages for the Admin interface, set "debug.admin", and for the
 * Customer interface, "debug.customer". Properties are typically set with the
 * -D option to the java executable.
 */
import java.io.Console; // Only need this to start a session
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.util.Random;

public class Main {
    private static final String url
        = "jdbc:oracle:thin:@class3.cs.pitt.edu:1521:dbclass";

    static {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        }
        catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        Console c = System.console();

        c.printf("To exit, close the input stream with CTRL-D.%n");

        c.printf("Enter your username: ");
        String username = c.readLine();
        if (username == null) {
            return;
        }

        // Note: The password is kept in memory, so it's not the most secure
        // option, but it's better than hardcoding it in the program.
        char[] rawPassword = c.readPassword("Enter your password: ");
        if (rawPassword == null) {
            return;
        }
        String password = new String(rawPassword);
        for (int i = 0; i < rawPassword.length; ++i) {
            rawPassword[i] = (char)0;
        }

        c.printf("Log in as [A]dministrator, [C]ustomer, or run the [B]enchmark: ");
        String r = c.readLine();

        AdminSession    as = new AdminSession(username, password, url);
        CustomerSession cs = new CustomerSession(username, password, url);

        // Enable debugging messages
        as.DEBUG = System.getProperty("debug")          != null
                || System.getProperty("debug.admin")    != null;
        cs.DEBUG = System.getProperty("debug")          != null
                || System.getProperty("debug.customer") != null;

        // Set Date_Info if it isn't already set.
        as.setSysTime(new java.sql.Date(System.currentTimeMillis()), false);

        if ("A".equalsIgnoreCase(r)) {
            as.run();
        }
        else if ("C".equalsIgnoreCase(r)) {
            cs.run();
        }
        else if ("B".equalsIgnoreCase(r)) {
            benchmark(as, cs);
        }
        else {
            c.printf("Input not understood.%n");
            return; // Exit
        }
    }

    /**
     * Perform benchmarking. Benchmark the following functions:
     *   AdminSession:
     *     erase()
     *     loadAirline() \
     *     loadSchedule() } loadAll() calls these.
     *     loadPrice()   /
     *     loadPlane()  /
     *     makeManifest()
     *   CustomerSession:
     *     addCustomer()
     *     showCustomer()
     *     findPrice()
     *     findRoutes() (routes, routes with a single airline, routes with
     *     available seats, routes with available seats and a single airline)
     *     addReservation()
     *     showReservation()
     *     buyTicket()
     * @param as The AdminSession to benchmark.
     * @param cs The CustomerSession to benchmark.
     */
    public static void benchmark(AdminSession as, CustomerSession cs) {
        Console c = System.console();
        if (c == null) {
            System.out.println("Error: Couldn't get console object");
            return;
        }
        PrintWriter out = c.writer();

        StopWatch w = new StopWatch();

        // Erase data
        out.println("Testing the function to erase all data");
        out.println();
        w.start();
        as.erase(true);
        w.stop();
        out.println();
        out.printf(
            "Erasing the (presumably empty) tables took %d MS%n",
            w.millis()
        );
        w.reset();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        out.println("Loading data from the sample_data directory");
        out.println();
        w.start();
        as.loadAll("sample_data");
        w.stop();
        out.println();
        out.printf("Loading took %d MS%n", w.millis());

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        out.println("Testing the function to erase all data, now with data");
        out.println();
        w.start();
        as.erase(true);
        w.stop();
        out.println();
        out.printf(
            "Erasing the tables took %d MS%n",
            w.millis()
        );
        w.reset();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        // Need to set system date again since we erased everything
        as.setSysTime(new java.sql.Date(System.currentTimeMillis()), false);

        out.println("Loading data from the sample_data directory again");
        out.println();
        w.start();
        as.loadAll("sample_data");
        w.stop();
        out.println();
        out.printf("Loading took %d MS%n", w.millis());
        w.reset();

        boolean haveCustomers = false;
        boolean haveReservations = false;

        out.println("Loading sample customers");
        w.start();
        try {
            cs.loadSQL(new java.io.File("sample_data/customers.sql"));
            haveCustomers = true;
        }
        catch (java.io.IOException e) {
            out.println("Error reading customers: " + e.getMessage());
        }
        w.stop();
        out.printf("Loading customers took %d MS%n", w.millis());
        out.println();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }

        out.println("Loading sample reservations");
        w.start();
        try {
            cs.loadSQL(new java.io.File("sample_data/reservations.sql"));
            haveReservations = true;
        }
        catch (java.io.IOException e) {
            out.println("Error reading reservations: " + e.getMessage());
        }
        w.stop();
        out.printf("Loading reservations took %d MS%n", w.millis());
        out.println();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        out.println("Summary of data:");
        as.showSummary(false);

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        if (!haveCustomers || !haveReservations) {
            out.println(
                "There was a problem loading the sample data."
                + " Make sure the \"sample_data\" directory is located"
                + " in the same directory as the Main class."
            );
            return;
        }

        // Test makeManifest()
        out.println("Generating some flight manifests");
        try {
            as.makeManifest("001", Util.parseDate("10/10/2016"));
            as.makeManifest("004", Util.parseDate("08/26/2016"));
            as.makeManifest("012", Util.parseDate("04/02/2016"));
        }
        catch (java.text.ParseException e) {}
        out.println();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        // Test showCustomer()
        out.println("Showing some customers");
        cs.showCustomer("Oliver", "Lane");
        cs.showCustomer("Kendall", "Reeves");
        cs.showCustomer("Jin", "Bishop");
        cs.showCustomer("Juliet", "Olsen");
        cs.showCustomer("Bryar", "Simmons");
        cs.showCustomer("Clarke", "Rowe");
        out.println();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        // Test showReservation()
        out.println("Showing some reservations");
        for (int i = 0; i < 5; ++i) {
            cs.showReservation(String.format("%03d", i + 1));
        }
        out.println();

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        // Test buyTicket()
        out.println("Buying tickets for reservations 1 through 50...");
        long sum = 0;
        for (int i = 0; i < 50; ++i) {
            w.reset();
            w.start();
            cs.buyTicket(i + 1);
            w.stop();
            sum += w.millis();
        }
        out.printf(
            "Done after %d milliseconds, average of %d per purchase%n%n",
            sum,
            sum / 50
        );

        if (!as.menu.promptYesOrNo("Continue?")) {
            return;
        }
        out.println();

        // Test findRoutes()
        String[] airports = {
            "BHM", "DHN", "HSV", "MOB", "MGM", "ANC", "ANI", "BRW", "BET",
            "CDV", "SCC", "DLG", "FAI", "GAL", "GST", "HNS", "HOM", "HNH",
            "JNU", "ENA", "KTN", "AKN", "ADQ", "OTZ", "OME", "PSG", "SIT",
            "KSM", "UNK", "DUT", "VDZ", "WRG", "YAK", "IFP", "FLG", "GCN",
            "AZA", "PGA"
        };
        out.println("Testing the four route finding functions on random paths");
        Random r = new Random();
        String airline = "Allegiant Air";
        java.sql.Date date = new java.sql.Date(1481346000);
        w.reset();
        w.start();
        for (int i = 0; i < 20; ++i) {
            String cityA = airports[r.nextInt(airports.length)];
            String cityB = airports[r.nextInt(airports.length)];
            out.printf("Routes from %s to %s%n", cityA, cityB);
            cs.findRoutes(
                false, false,
                cityA, cityB,
                airline,
                date
            );
            out.printf("Routes specific to airline \"%s\"%n", airline);
            cs.findRoutes(
                true, false,
                cityA, cityB,
                airline,
                date
            );
            out.printf("Routes from %s to %s with open seats%n", cityA, cityB);
            cs.findRoutes(
                false, true,
                cityA, cityB,
                airline,
                date
            );
            out.printf("Routes with open seats specific to airline \"%s\"%n", airline);
            cs.findRoutes(
                true, true,
                cityA, cityB,
                airline,
                date
            );
        }
        w.stop();
        out.printf(
            "%nTook %d MS to search for 20 routes with 4 different"
           + " criteria per search%n",
           w.millis()
        );
    }
}
