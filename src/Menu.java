import java.io.Console;
import java.io.PrintWriter;

/**
 * Implementation of a menu. This class wraps a Console object and its
 * underlying PrintWriter, which offers more flexibility (and less typing)
 * compared to using System.out. Regarding flexibility, PrintWriter is designed
 * for writing character-based (encoded) output, while PrintStreams (like
 * System.out) are designed to write unencoded bytes. It also allows us to
 * set {@code out} to print to different locations than standard output, e.g.
 * for testing purposes.
 */
public class Menu<E extends Enum<E>> {

    private E[] items;

    private final Console console;

    public final PrintWriter out;

    /**
     * Create a new Menu.
     * @param e An array of enum values representing the menu items.
     * @param c The console which the menu should be printed to.
     */
    public Menu(E[] e, Console c) {
        items = Util.requireNonNull(e);
        if (c == null) {
            throw new RuntimeException(
                "Did not receive a Console in Menu constructor"
            );
        }
        console = c;
        out = console.writer();
    }

    /**
     * Draw the menu.
     */
    public void draw() {
        out.println();
        out.println("Select an action from the list.");
        for (E e : items) {
            out.printf("%s%n", e);
        }
        out.println();
    }

    /**
     * Clear the console. Uses an ANSI escape code, may not be portable.
     */
    public void clear() {
        out.print("\033\143");
    }

    /**
     * Prompt for a number from the user, read it, and return its corresponding
     * menu option.
     * @return The option selected by the user, or null if the number is not the
     * number of a valid option.
     */
    public E getOpt() {
        Integer r = promptNum("");
        if (r == null) {
            out.println("Please enter the number of an action.");
            return null;
        }
        --r;
        if (r < 0 || r >= items.length) {
            out.println(
                "Sorry, the number given was not that of a valid action."
                + " Please try again."
            );
            return null;
        }
        return items[r];
    }

    /**
     * Read a line from the current console, exiting if the input stream is
     * closed.
     * @return The line read from stdin.
     */
    public String getLineChecked() {
        String line = console.readLine();
        if (line == null) {
            out.println("Input closed, exiting.");
            System.exit(0);
        }
        return line;
    }

    /**
     * Display a prompt asking for a value, and return the user's answer.
     * <code>prompt()</code> will append a single colon (':') and a space to
     * prompt messages, unless the message is the empty string.
     * @param message The message to display in the prompt.
     * @return The user's response, never null.
     */
    public String prompt(String message) {
        if (message.length() != 0) {
            out.printf("%s: ", message);
        }
        return getLineChecked().trim();
    }

    /**
     * Prompt for a string with a maximum length, retrying on bad inputs.
     * @param message The message to display in the prompt.
     * @param maxLen The maximum length of the string to be returned.
     * @return The user's response, never null.
     */
    public String prompt(String message, int maxLen) {
        while (true) {
            String rsp = prompt(message);
            if (rsp.length() > maxLen) {
                out.println(
                    "Input cannot be longer than " + maxLen
                    + " characters, please try again."
                );
            }
            else {
                return rsp;
            }
        }
    }

    /**
     * Prompt the user for an integer value.
     * @param message The message to print in the prompt.
     * @return When successful, return the integer entered by the user. If the
     * entered message is the empty string, return <code>null</code>. If the
     * number fails to parse, print and error and continue trying until a valid
     * or empty input is given.
     */
    public Integer promptNum(String message) {
        while (true) {
            String rsp = prompt(message);
            if (rsp.length() == 0)
                return null;
            Integer i;
            try {
                i = Integer.parseInt(rsp);
            }
            catch (NumberFormatException e) {
                out.println("Input not understood.");
                continue;
            }
            return i;
        }
    }

    /**
     * Prompt for a yes or no answer. If the response starts with the letter
     * 'y', the answer is considered to be true, otherwise false.
     */
    public boolean promptYesOrNo(String message) {
        out.printf("%s [y/n]: ", message);
        String r = getLineChecked().trim();
        return r.startsWith("y");
    }

    /**
     * Prompt for a date in MM/DD/YYYY format. If the empty string is entered,
     * return {@code null}. If the entered date is otherwise invalid, retry.
     */
    public java.sql.Date promptDate(String message) {
        RETRY:
        while (true) {
            String r = prompt(message);
            if (r.length() == 0) {
                return null;
            }
            try {
                return Util.parseDate(r);
            }
            catch (java.text.ParseException e) {
                out.println("Date must be in MM/DD/YYYY format.");
                continue RETRY;
            }
        }
    }
}
