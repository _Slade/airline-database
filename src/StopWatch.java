public class StopWatch {
    private long start = 0, stop = 0, lastTime = 0;
    private boolean running = false;

    public void start() {
        if (running) {
            throw new RuntimeException("StopWatch already started");
        }
        start   = System.nanoTime();
        running = true;
    }

    public void stop() {
        if (!running) {
            throw new RuntimeException ("Cannot stop non-started StopWatch");
        }
        stop    = System.nanoTime();
        running = false;
    }

    public long nanos() {
        return stop - start;
    }

    public long millis() {
        return (stop - start) / 1000000;
    }

    public long s() {
        return (stop - start) / 1000000000;
    }

    public void reset() {
        running  = false;
        start    = 0;
        stop     = 0;
        lastTime = 0;
    }

    public static StopWatch newStarted() {
        StopWatch s = new StopWatch();
        s.start();
        return s;
    }
}
