import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * A class of utility functions that don't belong anywhere else, or which belong
 * to built-in classes but are backported here for compatibility.
 */
public class Util {

    private Util() { /* Not instantiable */ }

    public static final String NL = System.getProperty("line.separator");

    /**
     * Throw an exception if {@code obj} is null, otherwise return it.
     */
    public static <T> T requireNonNull(T obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        return obj;
    }

    /**
     * Throw an exception with the message {@code message} if {@code obj} is
     * null, otherwise return it.
     */
    public static <T> T requireNonNull(T obj, String message) {
        if (obj == null) {
            throw new NullPointerException(message);
        }
        return obj;
    }

    /**
     * Join a list of CharSequences with a delimiter.
     * Reimplemented here due to not being in JDKs earlier than 1.8
     * @param delim The delimiter that goes between each joined element.
     * @param ss The list of elements to be joined.
     */
    public static String join(CharSequence delim, CharSequence... ss) {
        requireNonNull(delim);
        requireNonNull(ss);
        return join(delim, Arrays.asList(ss));
    }

    /**
     * Join an Iterable of CharSequences with a delimiter.
     * @param delim The delimiter that goes between each joined element.
     * @param ss The Iterable of CharSequences to be joined.
     */
    public static String join(CharSequence delim, Iterable<CharSequence> ss) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : ss) {
            if (sb.length() != 0) {
                sb.append(delim);
            }
            sb.append(s);
        }
        return sb.toString();
    }

    /**
     * Join a list of CharSequences with newlines.
     * @param ss The Iterable of CharSequences to be joined.
     */
    public static String joinLines(CharSequence... ss) {
        return join(NL, ss);
    }

    /**
     * Join an Iterable of CharSequences with newlines.
     * @param ss The Iterable of CharSequences to be joined.
     */
    public static String joinLines(Iterable<CharSequence> ss) {
        return join(NL, ss);
    }

    /**
     * Return a string consisting of the CharSequence {@code s} repeated
     * {@code n} times.
     */
    public static String repeatString(CharSequence s, int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            sb.append(s);
        }
        return sb.toString();
    }

    /**
     * Return a list of an object {@code t} repeated {@code n} times.
     */
    public static <T> List<T> repeatList(T t, int n) {
        List<T> list = new ArrayList<T>(n);
        for (int i = 0; i < n; ++i) {
            list.add(t);
        }
        return list;
    }

    /**
     * Return the smaller of two items. If the items compare equal, the first is
     * returned. If either input is {@code null}, return {@code null}.
     * {@code null} is returned.
     * @param a The first item.
     * @param b The second item.
     * @return The smaller of two.
     */
    public static <T extends Comparable<? super T>> T min(T a, T b) {
        if (a == null || b == null) {
            return null;
        }
        return (a.compareTo(b) <= 0) ? a : b;
    }

    /**
     * Return the larger of two items. If the items compare equal, the first is
     * returned. If either input is null, the other is returned.
	 * @param a The first item.
	 * @param b The second item.
	 * @return The larger of the two.
     **/
    public static <T extends Comparable<? super T>> T max(T a, T b) {
        if (a == null || b == null) {
            return null;
        }
        return (a.compareTo(b) > 0) ? a : b;
    }

    /** Parse a date in the format MM/DD/YYYY and return an SQL Date object. */
    public static Date parseDate(String d) throws ParseException {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        java.util.Date utilDate = df.parse(d);
        return new Date(utilDate.getTime());
    }

    /**
     * Safely close an object with a {@code .close()} method. Unfortunately,
     * JDK1.6 doesn't have the {@code AutoCloseable} interface, and performing
     * all of the necessary checks every single time is awful.
     *
     * There has to be a better way of doing this.
     */
    public static void safeClose(java.io.Closeable a) {
        try {
            if (a != null) {
                a.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public static void safeClose(java.sql.Connection a) {
        try {
            if (a != null) {
                a.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public static void safeClose(java.sql.Statement a) {
        try {
            if (a != null) {
                a.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public static void safeClose(java.sql.ResultSet a) {
        try {
            if (a != null) {
                a.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Get the current date from the database.
     * @param username Username to log in with.
     * @param password Password to log in with.
     * @param url      URL to identify the database with.
     * @return The current date.
     * @throws SQLException If there is an error connecting to the database,
     * executing a query, or retrieving the result.
     */
    public static Date getSystemDate(
        String username,
        String password,
        String url
    ) throws SQLException {
        Connection con = null;
        Statement stmt = null;
        try {
            con = DriverManager.getConnection(url, username, password);
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT C_Date FROM Date_Info");
            if (rs.next()) {
                return rs.getDate(1);
            }
        }
        catch (SQLException e) {
            Util.safeClose(stmt);
            Util.safeClose(con);
            throw e;
        }
        return null;
    }

    /**
     * Get the current date from the database, using an existing connection.
     * @param con An open database connection.
     * @return The current date.
     * @throws SQLException If there is an error connecting to the database,
     * executing a query, or retrieving the result.
     */
    public static Date getSystemDate(
        java.sql.Connection con
    ) throws SQLException {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT C_Date FROM Date_INFO");
            if (rs.next()) {
                return rs.getDate(1);
            }
        }
        catch (SQLException e) {
            throw e;
        }
        return null;
    }
}
